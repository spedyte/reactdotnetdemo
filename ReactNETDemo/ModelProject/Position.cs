﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProject
{
    public class Position
    {
        public Position()
        {

        }

        [Key]
        public int PositionId { get; set; }

        [Required]
        public string PositionName { get; set; }

        [Required]
        public string PositionDescription { get; set; }

        [ForeignKey("CompanyId")]
        public int CompanyId { get; set; }

        public Company Company { get; set; }
    }
}
