﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProject
{
    public class UserApp
    {
        public UserApp()
        {

        }

        [Key]
        public int UserAppId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string ApPaterno { get; set; }

        [Required]
        public string ApMaterno { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Pwd { get; set; }

        [ForeignKey("PositionId")]
        public int PositionId { get; set; }

        public Position Position { get; set; }
    }
}
