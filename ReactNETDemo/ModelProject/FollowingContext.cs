﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProject
{
    public class FollowingContext : DbContext
    {
        public FollowingContext() :base("name=FollowingDBConnectionString")
        {

        }

        /*protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }*/

        public DbSet<Company> Companies { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<UserApp> UserApps{ get; set; }
        

    }
}
