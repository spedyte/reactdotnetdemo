﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    public class Note
    {

        public string UserNoteRespNickname { get; set; }
        public string UserNoteRegNickname { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NoteId { get; set; }

        public DateTime DateNoteReigster { get; set; }
        public bool IsAttended { get; set; }
        public string Content { get; set; }

        [ForeignKey("UserNoteRespNickname")]
        public virtual UserApp UserNoteResp { get; set; }

        [ForeignKey("UserNoteRegNickname")]
        public virtual UserApp UserNoteReg { get; set; }



        public int ProjectId { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int TemplateId { get; set; }
        public int MilestoneId { get; set; }

        [ForeignKey("ProjectId,Month,Year,TemplateId,MilestoneId")]
        public virtual MileStone Milestone { get; set; }
    }
}
