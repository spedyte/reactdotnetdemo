﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to exchange UserApp's information between 
    /// the system and the web browser clietns
    /// This kind of objects doesnt have references to other objects
    /// </summary>
    public class UserAppPOJO
    {
        public int UserAppId { get; set; }
        public int PositionId { get; set; }
        public int CompanyId { get; set; }
        public string NickName { get; set; }
        public string Pwd { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string SureName { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
    }
}
