﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to repesent milestone released in each deliver to the external clients
    /// Refrences to: Release, Templates,...
    /// </summary>
    public class MileStone
    {
        //properties to save the foregin keys
        [Key, Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MileStoneId { get; set; }

        [Key, Column(Order = 1)]
        public string Month { get; set; }
        [Key, Column(Order = 2)]
        public string Year { get; set; }
        [Key, Column(Order = 3)]
        public int TemplateId { get; set; }

        [Key, Column(Order = 0)]
        public int ProjectId { get; set; }

        public int NumVoBos { get; set; }
        public string Clave { get; set; }
        public string Name { get; set; }
        public bool isDigitalized { get; set; }
        public bool isOpened { get; set; }
        public string DigitalPath { get; set; }
        public int NumNotes { get; set; }

        [ForeignKey("Month,Year")]
        public virtual Release Release { get; set; }

        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<VoBo> VoBos { get; set; }

    }
}
