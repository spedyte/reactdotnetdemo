﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to represent each basic template required into the projects
    /// In this moment each template it is a word document
    /// References to: List of MileStones
    /// </summary>
    public class Template
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TemplateId { get; set; }
        
        public string Name { get; set; }
        public string Content { get; set; }
        public string Path { get; set; }

        public virtual ICollection<MileStone> MileStones { get; set; }
    }
}
