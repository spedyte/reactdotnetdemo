﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    public class VoBo
    {
        //properties to save the foregin keys
        
        public string UserVoBoNickname { get; set; }
        public string UserRegisterNickname { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VoBoId { get; set; }

        public DateTime DateVoboReigster { get; set; }
        
        [ForeignKey("UserVoBoNickname")]
        public virtual UserApp UserVoBo { get; set; }

        [ForeignKey("UserRegisterNickname")]
        public virtual UserApp UserRegister { get; set; }


        
        public int ProjectId { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int TemplateId { get; set; }
        public int MilestoneId { get; set; }

        [ForeignKey("ProjectId,Month,Year,TemplateId,MilestoneId")]
        public virtual MileStone Milestone { get; set; }
    }
}
