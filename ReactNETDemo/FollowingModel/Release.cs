﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to represent a deliver to the external client
    /// References to: List of MileStones
    /// </summary>
    public class Release
    {
        //properties to save the foregin keys
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReleaseId { get; set; }

        [Key, Column(Order = 0)]
        public string Month { get; set; }
        [Key, Column(Order = 1)]
        public string Year { get; set; }

        public bool isClosed  { get; set; }

        public virtual ICollection<MileStone> MileStones { get; set; }
    }
}
