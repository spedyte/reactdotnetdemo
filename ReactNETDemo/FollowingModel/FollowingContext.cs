﻿using BussinesProject;
using Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created bay Armando.Castaneda - 20170503
    /// Class to create the DB context used on the web application
    /// It contains all the tables that are needed for the system using
    /// </summary>
    public class FollowingContext : DbContext
    {
        public FollowingContext() :base("name=FollowingDB")
        {
            Database.SetInitializer<FollowingContext>(new FollowingDBInitializer<FollowingContext> ());
        }

        public virtual DbSet<Company> Companies { get; set; }

        public virtual DbSet<Position> Positions { get; set; }

        public virtual DbSet<UserApp> UsersApp { get; set; }

        public virtual DbSet<SystemApp> SystemsApp { get; set; }

        public virtual DbSet<Project> Projects { get; set; }

        public virtual DbSet<MileStoneNote> MileStoneNotes { get; set; }

        public virtual DbSet<Release> Releases { get; set; }

        public virtual DbSet<MileStone> MileStones { get; set; }

        public virtual DbSet<Template> Templates { get; set; }

        public virtual DbSet<VoBo> VoBos { get; set; }

        public virtual DbSet<Note> Notes { get; set; }

        private class FollowingDBInitializer<T> : DropCreateDatabaseIfModelChanges<FollowingContext>
        {
            protected override void Seed(FollowingContext context)
            {
                try
                {

                    using(var dbContextTransaction = context.Database.BeginTransaction()){
                        //we add companies
                        IList<Company> companies = new List<Company>();
                        companies.Add(new Company() { Name = "Ultrasist", Description = "Consultant Software Development Company", isIntern = true });
                        companies.Add(new Company() { Name = "Infovissste", Description = "Social Insurance Goverment Company", isIntern = false });

                        foreach (var company in companies)
                        {
                            context.Companies.Add(company);
                        }

                        //we add positions
                        IList<Position> positions = new List<Position>();
                        positions.Add(new Position() {  Name = "Administrator", Description = "Person who administratives the entire site" });
                        positions.Add(new Position() {  Name = "Manager", Description = "Person who manages the project" });
                        positions.Add(new Position() { Name = "Documentator", Description = "Person who creates, modifies or deletes project documentation" });
                        //positions.Add(new Position() {  Name = "Developer", Description = "Person who write the software solution" });
                        //positions.Add(new Position() {  Name = "Analyst", Description = "Person who analyses the problme to create a solution" });
                        

                        foreach (var position in positions)
                        {
                            context.Positions.Add(position);
                        }

                        //we add users
                        IList<UserApp> usersapp = new List<UserApp>();

                        usersapp.Add(new UserApp()
                        {
                            Name = "Ana Luisa",
                            LastName = "Rios",
                            SureName = "Flores",
                            PositionId = 1,
                            CompanyId = 1,
                            NickName = "admin",
                            Pwd = SecurityHelper.Encrypt("adminriosfl"),
                            Avatar = "avatar6.png",
                            Email = "ariosf@ultrasist.com.mx"
                        });
                        usersapp.Add(new UserApp()
                        {
                            Name = "Ana Luisa",
                            LastName = "Rios",
                            SureName = "Flores",
                            PositionId = 2,
                            CompanyId = 1,
                            NickName = "anariosfl",
                            Pwd = SecurityHelper.Encrypt("anariosfl"),
                            Avatar = "avatar6.png",
                            Email = "ariosf@ultrasist.com.mx"
                        });

                        foreach (var userapp in usersapp)
                        {
                            context.UsersApp.Add(userapp);
                        }

                        //we add Systems
                        /*IList<SystemApp> systems = new List<SystemApp>();
                        systems.Add(new SystemApp() { Clave = "SIOP", Description = "SIOP System", Name = "Sistema Integral Oxxxxx Pxxxxxx" });
                        systems.Add(new SystemApp() { Clave = "SIO", Description = "SIO System", Name = "Sistema Integral Occccc" });
                        foreach (var systemapp in systems)
                        {
                            context.SystemsApp.Add(systemapp);
                        }*/
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }


                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {

                        //we add projects
                        /*IList<Project> projects = new List<Project>();
                        projects.Add(new Project()
                        {
                            Aka = "AliPlus",
                            Clave = "WO94850",
                            Name = "Aliados-Plus-Modificaciones Gráfica Base",
                            SystemAppId = 1,
                            StartDate = new DateTime(2016, 7, 11),
                            EndDate = new DateTime(2018, 5, 11)
                        });

                        projects.Add(new Project()
                        {
                            Aka = "DigiTramites",
                            Clave = "WO94830",
                            Name = "Digitalización de Trámites Modificaciones Gráfica Base",
                            SystemAppId=2,
                            StartDate = new DateTime(2016, 5, 22),
                            EndDate = new DateTime(2018, 8, 22)
                        });
                        foreach (var project in projects)
                        {
                            context.Projects.Add(project);
                            context.SaveChanges();
                        }

                        //we add releases
                        IList<Release> releases = new List<Release>();
                        releases.Add(new Release() {  Month = "Diciembre", Year = "2016", isClosed = false });

                        foreach (var release in releases)
                        {
                            context.Releases.Add(release);
                            context.SaveChanges();
                        }*/

                        IList<Template> templates = new List<Template>();
                        templates.Add(new Template() {  Name = "Guía de Adaptación del Proyecto", Path = "guiaAdapTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Solicitud de Servicio", Path = "solServicioTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Programa de trabajo", Path = "progTrabajoTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Doc. De Presupuestos de Horas Hombre", Path = "docHrsHombreTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Análisis y Diseño de Requerimiento", Path = "analDisenoReqsTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Revisión de Calidad de Base de Datos", Path = "revCalidadBDTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Documento de Diseño", Path = "docDisenoTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Matriz de Casos de Prueba Diseño", Path = "matrizCasosPruebaDisenoTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Matriz de Prueba Diseño", Path = "matrizPruebaDisenoTemplate.docx", Content = "" });
                        templates.Add(new Template() {  Name = "Documento de Resultados de las Revisiones de Avance", Path = "docResultRevsAvanceTemplate.docx", Content = "" });
                        templates.Add(new Template() { Name = "Reporte de avance del proyecto-presentación ejecutiva", Path = "reportePreseEjecutivaTemplate.docx", Content = "" });
                        foreach (var template in templates)
                        {
                            context.Templates.Add(template);
                            context.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    //we update the DB
                    //base.Seed(context);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);                    
                }
               
            }
        }

    }
}
