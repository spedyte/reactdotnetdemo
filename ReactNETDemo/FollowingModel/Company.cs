﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to represent a company inside the web application,
    /// References to: List of UsersApp
    /// </summary>
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool isIntern { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<UserApp> UsersApp { get; set; }
    }
}
