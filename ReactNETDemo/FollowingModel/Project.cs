﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Class to represent each project inside 
    /// the service give to the external clients
    /// References to: SystemApp,
    /// </summary>
    public class Project
    {
        //properties to save the foregin keys
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectId { get; set; }
        public int SystemAppId { get; set; }
                
        public string Clave { get; set; }
        public string Name { get; set; }

        public string Aka { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [ForeignKey("SystemAppId")]
        public virtual SystemApp Sistema { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<MileStone> Mileston { get; set; }
    }
}
