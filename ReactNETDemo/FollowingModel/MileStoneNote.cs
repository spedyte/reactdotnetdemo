﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to keep notes about the milestones, any participant in the
    /// project can creates note reltaed to the milestones
    /// References to: UserApp
    /// </summary>
    public class MileStoneNote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MileStoneNoteId { get; set; }
        public string Message { get; set; }
        public DateTime RegisterDate { get; set; }
        public string NoteOwner { get; set; }
        public string NoteResponsable { get; set; }

        [ForeignKey("NoteOwner")]
        public virtual UserApp Owner { get; set; }

        [ForeignKey("NoteResponsable")]
        public virtual UserApp Responsable { get; set; }

    }
}
