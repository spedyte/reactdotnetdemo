﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowingModel
{
    /// <summary>
    /// Created by Armando.Castaneda - 20170503
    /// Class to represent an user of the system, these users are internal employees
    /// References to: Position,Company,List of MileStonesNotes
    /// </summary>
    public class UserApp
    {

        //properties to save the foregin keys
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserAppId { get; set; }

        public int PositionId { get; set; }
        public int CompanyId { get; set; }
        
        [Key]
        public string NickName { get; set; }
        public string Pwd { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string SureName { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }

        [ForeignKey("PositionId")]
        public virtual Position Position { get; set; }

        [ForeignKey("CompanyId")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual Company Company { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<MileStoneNote> MileStoneNotes { get; set; }

    }
}
