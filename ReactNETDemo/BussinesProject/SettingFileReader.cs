﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesProject
{
    public class SettingFileReader
    {
        public IEnumerable<string[]> ReadTXTSettingFile(string path, string settingfileName)
        {

            string assetFile =
                System.IO.Path.Combine(path, "SettingsFiles", settingfileName);


            Stream filestream = new FileStream(assetFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (filestream)
            {
                Encoding encoding = Encoding.UTF8;
                string content;
                using (StreamReader sr = new StreamReader(filestream, encoding))
                {
                    content = sr.ReadToEnd();
                }
                string[] lines = content.Split(';');
                return from line in lines
                       select (line.Split(',')).ToArray();
            }
        }
    }
}
