﻿
using Novacode;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesProject
{
    public class DocxOperations
    {
        public void CreateCaratulas(string outputFileNamex, string fileName,Dictionary<string,string> values)
        {
            // We will need a file name for our output file (change to suit your machine):
            string fileNameTemplate = @outputFileNamex+ "-{0}.docx";// @"D:\Users\John\Documents\Rejection-Letter-{0}-{1}.docx";

            // Let's save the file with a meaningful name, including the applicant name and the letter date:
            string outputFile = string.Format(fileNameTemplate, DateTime.Now.ToString("MM-dd-yy"));

            // Grab a reference to our document template:
            DocX caratula = DocX.Load(fileName);

            foreach (var item in values)
            {
                // Perform the replace:
                caratula.ReplaceText(item.Key, item.Value);
            }
          
            // Save as New filename:
            caratula.SaveAs(outputFile);

            // Open in word:
            //Process.Start("WINWORD.EXE", "\"" + outputFile + "\"");
        }
    }
}
