﻿var flagIsLogged = false;
var iisSiteName = "";

function verifySession() {
    console.log(0,"Verifying session...");
    var activeSession = JSON.parse(localStorage.getItem('activeUsr'));
    if (!activeSession) {
        //window.location = 'login.html';
        flagIsLogged = false;
        //ReactDOM.render(<div></div>, document.getElementById('shortcuts'));
        //ReactDOM.render(<div></div>, document.getElementById('notifications'));
        //ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        //ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        //ReactDOM.render(<div></div>, document.getElementById('percentages'));
        //ReactDOM.render(<div></div>, document.getElementById('clasified'));
        ReactDOM.render(<div></div>,document.getElementById('leftmenu'));
        ReactDOM.render(<LoginBox pollInterval={2000} />, document.getElementById('content'));
        //ReactDOM.render(<div></div>, document.getElementById('myDashboardTitle'));
        ReactDOM.render(<EmptyProfile />, document.getElementById('profile'));

    }
    else {
        flagIsLogged = true;
        ReactDOM.render(<LeftMenuContainer pollInterval={10000} />,document.getElementById('leftmenu'));

        ReactDOM.render(<FullProfile user={activeSession} />, document.getElementById('profile'));
        ReactDOM.render(<MainContainer />, document.getElementById('content'));

        /*var userType = "";
        if (activeSession.usrRol == 'administrator') {
        document.getElementById('adminLink').style.display = 'visible';
        userType = " (admin)";
        }
        else {
        document.getElementById('adminLink').style.display = 'none';
        }
        var arrayURL = window.location.href.split('\/');
        if (activeSession.usrRol != 'administrator' && arrayURL[arrayURL.length - 1] == "newComics.html") {
        window.location = 'login.html';
        }
        document.getElementById('activeUsrName').textContent = activeSession.usrName + userType;
        */
        }
    }

    function closeSession() {
        localStorage.clear();
        //window.location = "index.html";
        console.log(1, 'debo ir al inicio');
        //ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        //ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        //ReactDOM.render(<div></div>, document.getElementById('percentages'));
        //ReactDOM.render(<div></div>, document.getElementById('clasified'));
        ReactDOM.render(<div></div>,document.getElementById('leftmenu'));
        ReactDOM.render(<LoginBox pollInterval={2000} />, document.getElementById('content'));
        //ReactDOM.render(<div></div>, document.getElementById('myDashboardTitle'));
        ReactDOM.render(<EmptyProfile />, document.getElementById('profile'));
    }

    function createSession(user)
    {
        if (user != null)
        {
            localStorage.setItem("activeUsr", JSON.stringify(user));
            console.log("session created...");
        }
        verifySession();

        /*var validUsrs = JSON.parse( getItem('users'));
        validUsrs.push({
            'userId': validUsrs.length + 1,
            'usrName': document.getElementById('txtName').value,
            'usrPwd': document.getElementById('txtPwd').value,
            'usrEmail': document.getElementById('txtEmail').value,
            'usrRol': 'user'
        });
        localStorage.setItem('users', JSON.stringify(validUsrs));*/
    }

    window.onload = verifySession;

    function showErrorMessage(title, message, time) {
        swal({
            title:title,
            text: message,
            timer: time,
            type: "error",
            showConfirmButton: false
        });
    }

    function showSuccessMessage(title, message, time) {
        swal({
            title: title,
            text: message,
            timer: time,
            type: "success",
            showConfirmButton: false
        });
    }


    function showMessageHTMLFormated(title,message,typeMsg) {
       swal({
          title: '<strong>'+title+'</strong>!',
          text: message,
          html: true,
          type: typeMsg,
          showConfirmButton: true
       });
        //&quot;
    }
