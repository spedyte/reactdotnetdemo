﻿class SystemsList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={systems:[]};
    }

    componentWillMount() {
        let urlSubmitComment = Router.action('SystemApp', 'GetAllSystems');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("ListSystems:: gather systems...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ systems: data });
                }
                else {
                    console.log("ListSystems:: error hathering systems...");
                }
            }
        }
        xhr.open('get', urlSubmitComment, true);
        xhr.send()
    }

    render(){

        const systemsContentList = this.state.systems.map((v, i) => {

            if(i%2==0)
                return(
                <tr key={i}>
                        <td><i className="fa fa-laptop w3-text-orange w3-large"></i></td>
                        <td>{v.Name}</td>
                        <td><i>{v.Clave}</i></td>
                    </tr>
                );
    else

            return(
                <tr key={i}>
                        <td><i className="fa fa-laptop w3-text-green w3-large"></i></td>
                         <td>{v.Name}</td>
                        <td><i>{v.Clave}</i></td>
                    </tr>
                );
                        });



                        return(<tbody>
                               <tr><th></th><th>Name</th><th>Clave</th></tr>
                        {systemsContentList}            
                               </tbody>);
                        }
}


class SystemsListContainer extends React.Component {
    render() {
        /*var styleComponent = {
            padding_top: "22px"
        };*/
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="systemListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable ">
                            <SystemsList />
                       </table>
                   </div>

                </div>
               );
    }
}