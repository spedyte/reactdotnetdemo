﻿class NewUserForm extends React.Component{
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        console.log("setup1",userSession.Avatar);
        this.state = {
            nickname:'',
            name: '',
            lastName: '',
            sureName: '',
            nickName: '',
            avatar: '',
            email: '',
            pwd: '',
            position: 0,
            company: 0,
            valdiationMsg:''
        };

        this.handlePwdChange = this.handlePwdChange.bind(this);
        this.handleNickNameChange = this.handleNickNameChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSureNameChange = this.handleSureNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleAvatarChange = this.handleAvatarChange.bind(this);
        this.handleFigureSelection = this.handleFigureSelection.bind(this);
        this.handleCompanySelection = this.handleCompanySelection.bind(this);
        this.handlePositionSelection = this.handlePositionSelection.bind(this);
    }

    handleCompanySelection(companySelected) {
        this.setState({ company: companySelected });
    }

    handlePositionSelection(positionSelected) {
        this.setState({ position: positionSelected });
    }


    handleFigureSelection(avatarSelected) {
        this.setState({ avatar: avatarSelected });
    }

    handlePwdChange(e) {
        this.setState({ pwd: e.target.value });
    }

    handleNickNameChange(e) {
        this.setState({ nickname: e.target.value });
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleLastNameChange(e) {
        this.setState({ lastName: e.target.value });
    }

    handleSureNameChange(e) {
        this.setState({ sureName: e.target.value });
    }

    handleEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleAvatarChange(e) {
        this.setState({ avatar: e.target.value });
    }


    handleSubmit(e) {
        e.preventDefault();
        var nickname = this.state.nickname.trim();
        var pwd = this.state.pwd.trim();
        /*if (!nickname || !pwd) {
            return;
        }*/
        if (handleValidationsNUser(this.state)) {
            // send request to the server

            this.props.onUserSubmit({
                NickName: this.state.nickname,
                Pwd: this.state.pwd,
                Name: this.state.name,
                LastName: this.state.lastName,
                SureName: this.state.sureName,
                Avatar: this.state.avatar,
                Email: this.state.email,
                Company: this.state.company,
                Position: this.state.position
            });
            this.setState({
                name: '',
                lastName: '',
                sureName: '',
                nickName: '',
                avatar: '',
                email: '',
                pwd: '',
                Avatar: '',
                position: 0,
                company: 0
            });
        }
        else {
            return;
        }
    }

    render() {
        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, fill the new User information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.name} placeholder="Name" onChange={this.handleNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Last Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.lastName} placeholder="Last name" onChange={this.handleLastNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Sure Name:</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.sureName} placeholder="Sure Name" onChange={this.handleSureNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Email ::</strong>
                        </td>
                        <td>
                            <input type="email" value={this.state.email} placeholder="Email" onChange={this.handleEmailChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                           <strong>Select the Company:</strong>
                        </td>
                        <td>
                             <CompanySelector companySelected={this.handleCompanySelection} />
                        </td>
                    </tr>
                     <tr>
                         <td style={{width:"175px"}}></td>
                        <td>
                           <strong>Select the Rol:</strong>
                        </td>
                        <td>
                            <PositionSelector positionSelected={this.handlePositionSelection} />
                        </td>
                     </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Select an<br /> avatar:</strong>
                        </td>
                        <td>
                            <div id="avatarsSelection">
                                <AvatarSelector figureSelected={this.handleFigureSelection} createUser={true}/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Nick Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.nickame} placeholder="Nick Name" onChange={this.handleNickNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Pwd :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.pwd} placeholder="Password" onChange={this.handlePwdChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td></td>
                        <td>
                            <input type="submit" style={{width:"175px"}} value="Create User" />
                        </td>
                    </tr>
                </tbody>
            </table>
          </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"320px"}}></td>
                    <td>
                        <input type="button" style={{width:"175px"}} value="Cancel" onClick={this.handleCancel} />
                    </td>
                </tr>
            </tbody>
        </table>
            </div>

      );
            }
}

function validateEmail(email)   
{  
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        return (true)  
    }
    return (false)  
}

function handleValidationsNUser(data) {
    console.log(99, data);
    let nickname = data.nickname.trim();
    let pwd = data.pwd.trim();
    let name = data.name.trim();
    let lastName = data.lastName.trim();
    let sureName = data.sureName.trim();
    let avatar = data.avatar.trim();
    let email = data.email.trim();
    let position = data.position;
    let company = data.company;
    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!nickname && nickname.length < 7) {
        valdiationMsg = valdiationMsg + '<li><strong>NickName</strong> not NULL or must have at least 8 characters</li>';
        flags[0] = false;
    }
    console.log(0,"nickname validated");
    if (!name && name.length < 2 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Name</strong> not NULL, use only alpha characters and at least 3 in your name</li>';
        flags[1] = false;
    }
    console.log(1, "name validated");
    if (!pwd && pwd.length < 5 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Pwd</strong> not NULL, use only alphanumeric characters and at least 3 in your pwd</li>';
        flags[2] = false;
    }
    console.log(2, "pwd validated");
    if (!lastName && lastName.length < 2 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Last Name</strong> not NULL, use only alpha characters and at least 3 in your last name</li>';
        flags[3] = false;
    }
    console.log(3, "lastname validated");
    if (!sureName && sureName.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>SureName</strong> not NULL, use only alpha characters and at least 3 in your surename</li>';
        flags[4] = false;
    }
    console.log(4, "surename validated");
    if (!avatar && avatar.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Avatar</strong> not NULL, choose one</li>';
        flags[5] = false;
    }
    console.log(5, "avatar validated");
    if (!validateEmail(email)) {
        valdiationMsg = valdiationMsg + '<li><strong>Email</strong> not NULL, use only alpha characters and a valid email direction</li>';
        flags[6] = false;
    }
    console.log(6, "email validated");
    if (position == 0) {
        valdiationMsg = valdiationMsg + '<li>Choose a <strong>position</strong></li>';
        flags[7] = false;
    }
    console.log(7, "position validated");
    if (company==0) {
        valdiationMsg = valdiationMsg + '<li>Choose a <strong>company</strong></li>';
        flags[8] = false;
    }
    valdiationMsg = valdiationMsg + '</ul>';
    console.log(8, "company validated");
    console.log("flags",flags);
    if ((flags[0] && flags[1]) && (flags[2] && flags[3]) && (flags[4] && flags[5]) && (flags[6] && flags[7])&&(flags[8])) {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check your information", valdiationMsg, "error");
        return false;
    }

}


class NewUserFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleUserSubmit = this.handleUserSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("New User:: Cancel operation...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleUserSubmit(usr) {
        let data = new FormData();
        data.append('Name', usr.Name);
        data.append('LastName', usr.LastName);
        data.append('SureName', usr.SureName);
        data.append('Email', usr.Email);
        data.append('NickName', usr.NickName);
        data.append('Pwd', usr.Pwd);
        data.append('NewPwd', usr.NewPwd);
        data.append('Avatar', usr.Avatar);
        data.append('CompanyId', usr.Company);
        data.append('PositionId', usr.Position);

        console.log(111, data);
        let urlSubmitComment = Router.action('User', 'AddNewUser');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                waitingDialog.hide();
                if (xhr.responseText) {
                    console.log("New user:: gathered from the server...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showSuccessMessage("Done!", "User was added!", 2000);
                }
                else {
                    console.log("New User:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the user.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmitComment, true);
        xhr.send(data)

    }

    /*componentDidMount() {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    }*/

    render() {
        return (
          <div>
            <NewUserForm onUserSubmit={this.handleUserSubmit} onCancelAction={this.handleCancelUpdate} />
          </div>
      );
            }
}

