﻿class LoginForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = { nickname: '', pwd: '' };
        this.handleNickChange = this.handleNickChange.bind(this);
        this.handlePwdChange = this.handlePwdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNickChange(e) {
        this.setState({ nickname: e.target.value });
    }

    handlePwdChange(e) {
        this.setState({ pwd: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        var nickname = this.state.nickname.trim();
        var pwd = this.state.pwd.trim();
        if (!nickname || !pwd) {
            return;
        }
        // send request to the server
        this.props.onLoginSubmit({ NickName: nickname, Pwd: pwd });
        this.setState({ nickname: '', pwd: '' });
    }

    render() {
        return (
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, enter your information</strong> </span><br /><br />
            Enter your nickname: <input type="text"
                   placeholder="Your nickname"
                   value={this.state.nickname}
                   onChange={this.handleNickChange} /> 
            <br/><br />
            Enter your pwd : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password"
               placeholder="Your pwd..."
               value={this.state.pwd}
               onChange={this.handlePwdChange} />
           <br /><br />
           <input type="submit" value="Send" />
          </form>
      );
    }
}

class LoginBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        //this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    }


    handleLoginSubmit(usr) {
        var data = new FormData();
        data.append('NickName', usr.NickName);
        data.append('Pwd', usr.Pwd);
        
        var urlSubmitComment = Router.action('User', 'GetUser');
                
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    waitingDialog.hide();
                    console.log("user gathered from the server...");
                    var dataUser = JSON.parse(xhr.responseText);
                    if (dataUser != null) {
                        console.log("LOGIN:: user welcome again...");
                        showSuccessMessage("Welcome again!", "", 2000);
                        createSession(dataUser);
                    }
                    else {
                        console.log("LOGIN:: user doesnt exits...");
                        showErrorMessage("User doesn´t exist !", "Please, check your information.", 3000);
                    }
                    
                }
                else {
                    waitingDialog.hide();
                    console.log("LOGIN:: Error during the request...");
                    showErrorMessage("Something wrong happened!", "Please, try again in few minutes.", 3000);
               }
            }
        }
        xhr.open('post', urlSubmitComment, true);
        xhr.send(data)
    }

    /*componentDidMount() {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    }*/

    render() {
        return (
          <div>
            <LoginForm onLoginSubmit={this.handleLoginSubmit} />
          </div>
      );
    }
}

