﻿class ReleaseLMList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={releases:[]};
        this.handleClic = this.handleClic.bind(this);
    }

    handleClic(value) {
        if (value != null) {
            ReactDOM.render(<div></div>, document.getElementById('deliveriesManager'));
            localStorage.setItem("releaseSelected", JSON.stringify(value));
            showMessageHTMLFormated("Release selected", "<strong>" + value.Month + ' - ' + value.Year + "</strong>", "success");
            console.log("project selected...");
            ReactDOM.render(<DeliveriesListContainer height={"300px"} width={"800px" } />, document.getElementById('deliveriesManager'));
             ReactDOM.render(<CreatMilestonesFormBox />, document.getElementById('createmileStonesDiv'));           
    }
}

componentWillMount() {
    let urlSubmit = Router.action('Release', 'GetAllReleases');
    let self = this;
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
        if (this.readyState === this.DONE) {
            if (xhr.responseText) {
                var data = JSON.parse(xhr.responseText);
                self.setState({ releases: data });
            }
            else {
                console.log("ListReleases:: error hathering releases...");
            }
        }
    }
    xhr.open('get', urlSubmit, true);
    xhr.send()
}

render(){
    const releasesContentList = this.state.releases.map((v, i) => {
        return(<li key={i} >
                <a onClick={this.handleClic.bind(this,v)}>{v.Month} {v.Year}</a>
            </li>);
});
return(
        <div>{releasesContentList}</div>
    );
}
}


class ReleasesLeftMenuContainer extends React.Component {
    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
            <div className="dropdown">
                <a id="dLabelR" role="button" data-toggle="dropdown" className="w3-bar-item w3-button w3-padding " data-target="#" href="/page.html">
                    <i className="fa fa-calendar fa-fw"></i> Select a Release <span className="caret"></span>
                </a>
    		    <ul className="dropdown-menu multi-level scrollable-menu" role="menu" aria-labelledby="dropdownMenu">
                    <ReleaseLMList/>
    		    </ul>
             </div>
                   );
    }
}