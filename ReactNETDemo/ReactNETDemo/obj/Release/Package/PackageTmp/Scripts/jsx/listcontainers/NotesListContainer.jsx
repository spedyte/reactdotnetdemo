﻿class NotesList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = { notes: [], noteselected: {} };
        this.handleMarkNoteAsAttended = this.handleMarkNoteAsAttended.bind(this);
    }

    handleMarkNoteAsAttended(note) {
        console.log(111, note)

        let data = new FormData();

        data.append('noteId', note.NoteId);

        let urlSubmit = Router.action('Note', 'MarkNoteAsAttended');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {

            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Notes:: gather templates...");
                    var data = JSON.parse(xhr.responseText);
                    ReactDOM.render(<div></div>, document.getElementById('tableNotes'));
                    ReactDOM.render(<NotesList milestone={self.props.milestone }/>, document.getElementById('tableNotes'));
                }
                else {
                    console.log("Notes:: error hathering templates...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)
    }

    componentWillMount() {
        let data = new FormData();

        data.append('MileStoneId', this.props.milestone.MileStoneId);
        data.append('Month', this.props.milestone.Month);
        data.append('Year', this.props.milestone.Year);
        data.append('TemplateId', this.props.milestone.TemplateId);
        data.append('ProjectId', this.props.milestone.ProjectId);

        let urlSubmit = Router.action('Milestone', 'GetNotes');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
                
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Notes:: gather templates...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ notes: data });
                }
                else {
                    console.log("Notes:: error hathering templates...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)
    }

    render() {
        let notesContentList;

        if (this.state.notes == null || this.state.notes.length == 0) {
            notesContentList = () => { return <tr><td colSpan={3 }>Does not exist <strong>template</strong> for this project</td></tr> };
            }
        else
        notesContentList = this.state.notes.map((v, i) => {

        if (JSON.parse(localStorage.getItem('activeUsr')).NickName == v.UserNoteResp.NickName)
        {
            //notes are to the user
            if (i % 2 == 0) 
            {
                if (v.IsAttended)
                    return (
                        <tr key={i}>
                        <td><i className="fa fa-commenting w3-text-green w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-green w3-xlarge"></i></td>
                    </tr>);
                  else 
                    return (
                        <tr key={i}>
                        <td><i className="fa fa-commenting w3-text-green w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><a href="#"><i className="fa fa-eye w3-text-red w3-xlarge" onClick={this.handleMarkNoteAsAttended.bind(this,v)}></i></a></td>
                    </tr>);

            }
            else
            {
                if (v.IsAttended)
                return (
                    <tr key={i}>
                    <td><i className="fa fa-commenting w3-text-blue w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-green w3-xlarge"></i></td>
                    </tr>);
                else
                    return (
                    <tr key={i}>
                    <td><i className="fa fa-commenting w3-text-blue w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><a href="#"><i className="fa fa-eye w3-text-red w3-xlarge" onClick={this.handleMarkNoteAsAttended.bind(this,v)}></i></a></td>
                    </tr>);
                }
            //end notes are to the user
        }
        else {
            //Notes are not to the user
            if (i % 2 == 0) 
            {
                if (v.IsAttended)
                    return (
                        <tr key={i}>
                        <td><i className="fa fa-commenting w3-text-green w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-green w3-xlarge"></i></td>
                    </tr>);
                else 
                    return (
                        <tr key={i}>
                        <td><i className="fa fa-commenting w3-text-green w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-red w3-xlarge"></i></td>
                    </tr>);

            }
            else
            {
                if (v.IsAttended)
                return (
                    <tr key={i}>
                    <td><i className="fa fa-commenting w3-text-blue w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-green w3-xlarge"></i></td>
                    </tr>);
                else
                    return (
                    <tr key={i}>
                    <td><i className="fa fa-commenting w3-text-blue w3-small"></i></td>
                        <td>{v.UserNoteReg.Name + ' ' + v.UserNoteReg.LastName + ' ' + v.UserNoteReg.SureName}</td>
                        <td>{v.DateNoteReigster}</td>
                        <td>{v.UserNoteResp.Name + ' ' + v.UserNoteResp.LastName + ' ' + v.UserNoteResp.SureName}</td>
                        <td><textarea cols="100" rows="4" defaultValue={v.Content}></textarea></td>
                        <td><i className="fa fa-eye w3-text-red w3-xlarge"></i></td>
                    </tr>);
                }
            ////END Notes are not to the user
        }
    });
        
       

        return(<tbody>
        <tr>
            <th></th>
            <th>Note Registered by</th>
            <th>Date</th>
            <th>Note's responsable</th>
            <th>Content</th>
            <th>Attended</th>
        </tr>
    {notesContentList}            
        </tbody>);
            
    }
}


class NotesListContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleNewNote = this.handleNewNote.bind(this);
        this.state = { milestone: this.props.milestone };
    }

    handleCancel() {
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleNewNote() {
        ReactDOM.render(<div></div>, document.getElementById('tableVoBos'));
        ReactDOM.render(<NotesList milestone={this.props.milestone }/>, document.getElementById('tableNotes'));
        }

    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return (
            <div>
                <p className="w3-twothird"><br /><strong> Notes: {this.props.milestone.Name} - {this.props.milestone.Month} {this.props.milestone.Year}</strong>  <br /></p>
                <div className="w3-twothird" id="vobosListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable " id="tableNotes">
                            <NotesList milestone={this.props.milestone}/>
                       </table>
                   </div>
                </div>
                <div className="w3-twothird">
                    <br/><br/>
                    <NewNoteFormBox milestone={this.props.milestone} onCreateNote={this.handleNewNote} />
                </div>
                <p className="w3-twothird"><br /><br /></p>
                <div className="w3-twothird">
                <table>
                        <tbody>
                            <tr>
                                <td style={{width:"320px"}}></td>
                                <td>
                                    <input type="button" style={{width:"175px"}} value="Return to Main Screen" onClick={this.handleCancel} />
                                </td>
                            </tr>
                        </tbody>
                </table>
                </div>
                </div>
               );
                                }
}