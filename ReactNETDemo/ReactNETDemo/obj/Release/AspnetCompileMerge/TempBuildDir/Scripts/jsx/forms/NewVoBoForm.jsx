﻿class NewVoBoForm extends React.Component {
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.state = {
            userVoBoId: 0,
            userRegisterVoBoId: userSession.UserAppId
        };

        this.handleUserVoBoIdChange = this.handleUserVoBoIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleUserVoBoIdChange(userSelected) {
        this.setState({ userVoBoId: userSelected });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (handleValidationsNVoBo(this.state)) {
            // send request to the server

            this.props.onVoBoSubmit({
                UserVoBoId: this.state.userVoBoId
            });
            this.setState({
                userVoBoId: 0
            });

            ReactDOM.render(<UserVoBoSelector userVoBoSelected={this.handleUserVoBoIdChange } />, document.getElementById('tdUserVoBoSelector'));
            
        }
        else {
            return;
        }
    }

    render() {

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <table>
                <tbody>
                    <tr>
                         <td style={{width:"175px"}}>Select <strong>User </strong> who gives VoBo.</td>
                        <td>
                           
                        </td>
                        <td id="tdUserVoBoSelector">
                            <UserVoBoSelector userVoBoSelected={this.handleUserVoBoIdChange} />
                        </td>
                        <td><input type="submit" style={{width:"175px"}} value="Create  VoBo" /></td>
                    </tr>
                </tbody>
            </table>
          </form>
      </div>

      );
            }
}

function handleValidationsNVoBo(data) {
    let uservoboid = data.userVoBoId;

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (uservoboid==0) {
        valdiationMsg = valdiationMsg + '<li><strong>SELECT</strong> a USER</li>';
        flags[0] = false;
    }
    
    if ((flags[0]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check SELECT the user", valdiationMsg, "error");
        return false;
    }

}


class NewVoBoFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {},milestone: this.props.milestone};
        this.handleVoBoSubmit = this.handleVoBoSubmit.bind(this);
        this.onCreateVoBoHandle = this.onCreateVoBoHandle.bind(this);
    }

    onCreateVoBoHandle() {
        this.props.onCreateVoBo();
    }

    handleVoBoSubmit(vobo) {
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        let data = new FormData();
        data.append('mileStoneId', this.props.milestone.MileStoneId);
        data.append('userVoBoId', vobo.UserVoBoId);
        data.append('userRegisterId', userSession.UserAppId);
        data.append('releaseID', (JSON.parse(localStorage.getItem('releaseSelected'))).ReleaseId);

        let urlSubmit = Router.action('Milestone', 'AddNewVoBo');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            waitingDialog.hide();
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("New vobo:: gathered from the server...");
                    //ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    self.onCreateVoBoHandle();
                    showSuccessMessage("Done!", "VoBO was added!", 2000);
                }
                else {
                    console.log("New vobo:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the VoBo.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewVoBoForm onVoBoSubmit={this.handleVoBoSubmit} />
          </div>
      );
            }
}

