﻿class VoBoList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = { vobos: [], voboselected: {} };
        this.handleUpdateTemplate=this.handleUpdateTemplate.bind(this);
    }

    handleUpdateTemplate(template) {
        console.log("Showing setup form....3..2..1..0");
        //ReactDOM.render(<SetupUserFormBox template={template} />, document.getElementById('content'));
    }

    componentWillMount() {
        let data = new FormData();

        data.append('MileStoneId', this.props.milestone.MileStoneId);
        data.append('Month', this.props.milestone.Month);
        data.append('Year', this.props.milestone.Year);
        data.append('TemplateId', this.props.milestone.TemplateId);
        data.append('ProjectId', this.props.milestone.ProjectId);

        let urlSubmit = Router.action('Milestone', 'GetVoBos');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
                
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("VoBos:: gather templates...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ vobos: data });
                }
                else {
                    console.log("VoBos:: error hathering templates...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)
    }

    render() {
        let vobosContentList;

        if (this.state.vobos == null || this.state.vobos.length == 0) {
            vobosContentList = () => { return <tr><td colSpan={3 }>Does not exist <strong>template</strong> for this project</td></tr> };
            }
        else

            //alert("pal map size:::: " + this.state.deliveries.lengt);
            vobosContentList = this.state.vobos.map((v, i) => {
            
            if (i % 2 == 0) 
            {
                return (
                    <tr key={i}>
                    <td><i className="fa fa-thumbs-o-up w3-text-green w3-small"></i></td>
                    <td>{v.UserVoBo.Name + ' ' + v.UserVoBo.LastName + ' ' + v.UserVoBo.SureName}</td>
                    <td>{v.DateVoboReigster}</td>
                    <td>{v.UserRegister.Name + ' ' + v.UserRegister.LastName + ' ' + v.UserRegister.SureName}</td>
                </tr>);
            }
            else
            {
                return (
                    <tr key={i}>
                    <td><i className="fa fa-thumbs-o-up w3-text-blue w3-small"></i></td>
                    <td>{v.UserVoBo.Name + ' ' + v.UserVoBo.LastName + ' ' + v.UserVoBo.SureName}</td>
                    <td>{v.DateVoboReigster}</td>
                    <td>{v.UserRegister.Name + ' ' + v.UserRegister.LastName + ' ' + v.UserRegister.SureName}</td>
                    </tr>);
        }

    });
        
       

        return(<tbody>
        <tr>
            <th></th>
            <th>User's VoBo</th>
            <th>Date</th>
            <th>VoBo registered by</th>
        </tr>
    {vobosContentList}            
        </tbody>);
            
    }
}


class VoBosListContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleNewVoBo = this.handleNewVoBo.bind(this);
        this.state = { milestone: this.props.milestone };
    }

    handleCancel() {
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleNewVoBo() {
        ReactDOM.render(<div></div>, document.getElementById('tableVoBos'));
        ReactDOM.render(<VoBoList milestone={this.props.milestone }/>, document.getElementById('tableVoBos'));
    }

    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return (
            <div>
                <p className="w3-twothird"><br /><strong> Vo:Bo's {this.props.milestone.Name} - {this.props.milestone.Month} {this.props.milestone.Year}</strong>  <br /></p>
                <div className="w3-twothird" id="vobosListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable " id="tableVoBos">
                            <VoBoList milestone={this.props.milestone}/>
                       </table>
                   </div>
                </div>
                <div className="w3-twothird">
                    <br/><br/>
                    <NewVoBoFormBox milestone={this.props.milestone} onCreateVoBo={this.handleNewVoBo} />
                </div>
                <p className="w3-twothird"><br /><br /></p>
                <div className="w3-twothird">
                <table>
                        <tbody>
                            <tr>
                                <td style={{width:"320px"}}></td>
                                <td>
                                    <input type="button" style={{width:"225px"}} value="Return to Main Screen" onClick={this.handleCancel} />
                                </td>
                            </tr>
                        </tbody>
                </table>
                </div>
                </div>
               );
    }
}