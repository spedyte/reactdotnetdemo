﻿class DashboardHeader extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = { numNotifications: 0, numVoBos: 0 };
        this.updateState = this.updateState.bind(this);
    }

    updateState() {
        let data = new FormData();
        data.append('userNickName', JSON.parse(localStorage.getItem('activeUsr')).NickName);

        let urlSubmit = Router.action('Notification', 'GetNumNotesUnattended');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Dashboard:: num notes...");
                    let dataNotesSrv= JSON.parse(xhr.responseText);
                    //alert(data);
                    self.setState({ numNotifications: dataNotesSrv });
                }
                else {
                    console.log("Dashboard:: error num notes...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

        //count VoBos per Day
        urlSubmit = Router.action('Notification', 'GetNumVoBosPerDay');
        xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Dashboard:: num vobos...");
                    var dataNumNotesSrv = JSON.parse(xhr.responseText);
                    self.setState({ numVoBos: dataNumNotesSrv });
                }
                else {
                    console.log("Dashboard:: error num vobos...");
                }
            }
        }
        xhr.open('get', urlSubmit, true);
        xhr.send()
        //ReactDOM.render(<div></div>, document.getElementById('mainReactContainer'));
        //ReactDOM.render(<DashboardHeader />, document.getElementById('mainReactContainer'));
    }

    componentDidMount() {
        this.updateState();
        window.setInterval(this.updateState, 5000);
    }


    componentWillMount() {
        let data = new FormData();
        data.append('userNickName', JSON.parse(localStorage.getItem('activeUsr')).NickName);

        let urlSubmit = Router.action('Notification', 'GetNumNotesUnattended');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Dashboard:: num notes...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ numNotifications: data });
                }
                else {
                    console.log("Dashboard:: error num notes...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

        //count VoBos per Day
        urlSubmit = Router.action('Notification', 'GetNumVoBosPerDay');
        xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Dashboard:: num vobos...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ numVoBos: data });
                }
                else {
                    console.log("Dashboard:: error num vobos...");
                }
            }
        }
        xhr.open('get', urlSubmit, true);
        xhr.send()


    }

    render() {
        var styleComponent = {
            padding_top:"22px"
        };

            return (
              <div>
            <header className="w3-container" style={styleComponent}>
                <h5><b id="myDashboardTitle"><i className="fa fa-dashboard"></i> My Dashboard</b></h5>
            </header>

            <div className="w3-row-padding w3-margin-bottom" id="shortcuts">
                <div className="w3-quarter">
                    <div className="w3-container w3-red w3-padding-16">
                        <div className="w3-left"><i className="fa fa-comment w3-xxxlarge"></i></div>
                        <div className="w3-right">
                            <h3>{this.state.numNotifications}</h3>
                        </div>
                        <div className="w3-clear"></div>
                        <h4>Pending Own Notes!</h4>
                    </div>
                </div>
                <div className="w3-quarter">
                    <div className="w3-container w3-blue w3-padding-16">
                        <div className="w3-left"><i className="fa fa-thumbs-o-up w3-xxxlarge"></i></div>
                        <div className="w3-right">
                            <h3>{this.state.numVoBos}</h3>
                        </div>
                        <div className="w3-clear"></div>
                        <h4>Vo.Bo.'s reached today</h4>
                    </div>
                </div>
                <div className="w3-quarter">
                    <div className="w3-container w3-teal w3-padding-16">
                        <div className="w3-left"><i className="fa fa-share-alt w3-xxxlarge"></i></div>
                        <div className="w3-right">
                            <h3>0</h3>
                        </div>
                        <div className="w3-clear"></div>
                        <h4>---</h4>
                    </div>
                </div>
                <div className="w3-quarter">
                    <div className="w3-container w3-orange w3-text-white w3-padding-16">
                        <div className="w3-left"><i className="fa fa-users w3-xxxlarge"></i></div>
                        <div className="w3-right">
                            <h3>0</h3>
                        </div>
                        <div className="w3-clear"></div>
                        <h4>---</h4>
                    </div>
                </div>
            </div>
        </div>
           );
        }
}