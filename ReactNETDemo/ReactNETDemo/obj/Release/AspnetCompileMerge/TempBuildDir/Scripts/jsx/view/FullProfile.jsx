﻿class FullProfile extends React.Component {
    constructor(props)
    {
        super(props);
        this.handleCloseSessionClick = this.handleCloseSessionClick.bind(this);
        this.handleConfigurationClick = this.handleConfigurationClick.bind(this);
    }

    handleCloseSessionClick(){
        console.log("Closing session....3..2..1..0");
        localStorage.removeItem('activeUsr');
        closeSession();
    }

    handleConfigurationClick() {
        console.log("Showing setup user form....3..2..1..0");
        ReactDOM.render(<SetupUserFormBox />, document.getElementById('content'));
    }

    render() {
        let avatarRoute = iisSiteName +"w3images/" + this.props.user.Avatar;
        let emailRoute = "mailto:" + this.props.user.Email + "?Subject=Define_el_titulo_del_correo"
        console.log(100, flagIsLogged);
        console.log("loading profile component...");
        if (flagIsLogged) {
            return (
            <div>
                <div className="w3-col s4">
                    <img src={avatarRoute} className="w3-circle w3-margin-right" style={{ width: "46px" } }/>
                </div>

                <div className="w3-col s8 w3-bar">
                    <span>Welcome, <strong>{this.props.user.Name}</strong> </span><br />
                    <a href="#" className="w3-bar-item w3-button" title="Close Session" onClick={this.handleCloseSessionClick}><i className="fa fa-sign-out"></i></a>
                    <a href="#" className="w3-bar-item w3-button" title="Contactos"><i className="fa fa-user"></i></a>
                    <a href="#" className="w3-bar-item w3-button" title="Change your information" onClick={this.handleConfigurationClick}><i className="fa fa-cog"></i></a>
                </div>
            </div>
           );
        }
        else {
            return(<EmptyProfile/>);
        }
    }
}
