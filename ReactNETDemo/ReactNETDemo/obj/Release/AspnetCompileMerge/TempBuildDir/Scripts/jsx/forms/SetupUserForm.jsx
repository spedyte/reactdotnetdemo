﻿class SetupUserForm extends React.Component{
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        console.log("setup1",userSession.Avatar);
        this.state = {
            name: userSession.Name,
            lastName: userSession.LastName,
            sureName: userSession.SureName,
            nickName: userSession.NickName,
            avatar: userSession.Avatar,
            email: userSession.Email,
            pwd: userSession.Pwd,
            newPwd: ''
        };
        //this.handleNickChange = this.handleNickChange.bind(this);
        this.handlePwdChange = this.handlePwdChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSureNameChange = this.handleSureNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleNewPwdChange = this.handleNewPwdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleAvatarChange = this.handleAvatarChange.bind(this);
        this.handleFigureSelection = this.handleFigureSelection.bind(this);
    }

    handleFigureSelection(avatarSelected) {
        console.log(10000, avatarSelected);
        this.setState({ avatar: avatarSelected });
    }

    handlePwdChange(e) {
        this.setState({ pwd: e.target.value });
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleLastNameChange(e) {
        this.setState({ lastName: e.target.value });
    }

    handleSureNameChange(e) {
        this.setState({ sureName: e.target.value });
    }

    handleEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    handleNewPwdChange(e) {
        this.setState({ newPwd: e.target.value });
    }

    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleAvatarChange(e) {
        this.setState({ avatar: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        var nickname = this.state.nickName.trim();
        var pwd = this.state.pwd.trim();
        if (!nickname || !pwd) {
            return;
        }
        // send request to the server

        this.props.onUserSubmit({
            NickName: nickname,
            Pwd: pwd,
            Name:this.state.name,
            LastName:this.state.lastName,
            SureName:this.state.sureName,
            Avatar: this.state.avatar,
            Email: this.state.email,
            NewPwd:this.state.newPwd
        });
        this.setState({
            name:'',
            lastName: '',
            sureName: '',
            nickName: '',
            avatar: '',
            email: '',
            pwd: '',
            Avatar: '',
            newPwd: ''
        });
    }

    render() {
        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, update your information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Nickname: :</strong>
                        </td>
                        <td>
                            <span>{this.state.nickName}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Current pwd ::</strong>
                        </td>
                        <td>
                            <span>{this.state.pwd}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.name} onChange={this.handleNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Last Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.lastName} onChange={this.handleLastNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Sure Name:</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.sureName} onChange={this.handleSureNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Email ::</strong>
                        </td>
                        <td>
                            <input type="email" value={this.state.email} onChange={this.handleEmailChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Select an<br /> avatar:</strong>
                        </td>
                        <td>
                            <div id="avatarsSelection">
                                <AvatarSelector figureSelected={this.handleFigureSelection} createUser={false}/>
                            </div>
                        </td>
                    </tr>
                     <tr>
                         <td style={{width:"175px"}}></td>
                        <td colSpan="2"><strong>**If it is necessary</strong></td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>New Pwd :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.newPwd} onChange={this.handleNewPwdChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>

                        </td>
                        <td>
                            <input type="submit" style={{width:"180px"}} value="Update profile" />
                        </td>
                    </tr>
                </tbody>
            </table>  
        </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"175px"}}></td>
                    <td style={{width:"100px"}}></td>
                    <td>
                        <input type="button" style={{width:"180px"}} value="Cancel" onClick={this.handleCancel}/>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
      );
    }
}

class SetupUserFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleUserSubmit = this.handleUserSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("cancel update user...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleUserSubmit(usr) {
        let data = new FormData();
        data.append('Name', usr.Name);
        data.append('LastName', usr.LastName);
        data.append('SureName', usr.SureName);
        data.append('Email', usr.Email);
        data.append('NickName', usr.NickName);
        data.append('Pwd', usr.Pwd);
        data.append('NewPwd', usr.NewPwd);
        data.append('Avatar', usr.Avatar);

        console.log(111, data);
        let urlSubmitComment = Router.action('User', 'UpdateUser');
        console.log(2, urlSubmitComment);

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("user gathered from the server...");
                    showSuccessMessage("Done!", "Update was successfull!", 2000);
                    var dataUser = JSON.parse(xhr.responseText);
                    localStorage.setItem("activeUsr", JSON.stringify(dataUser));
                    verifySession();
                    //ReactDOM.render(<MainContainer />, document.getElementById('content'));
                }
                else {
                    console.log("Update error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying update the user.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmitComment, true);
        xhr.send(data)

    }

    /*componentDidMount() {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    }*/

    render() {
        return (
          <div>
            <SetupUserForm onUserSubmit={this.handleUserSubmit} onCancelAction={this.handleCancelUpdate} />
          </div>
      );
          }
}

