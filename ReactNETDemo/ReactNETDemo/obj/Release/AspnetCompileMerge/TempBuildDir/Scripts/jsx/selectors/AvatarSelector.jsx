﻿class AvatarSelector extends React.Component {
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.state = { avatars: [],avatarSelected:userSession.Avatar ,flag:false};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(figureId) {
        if (this.props.createUser)
            this.setState({ flag: true});
        this.setState({ avatarSelected: figureId });
        this.props.figureSelected(figureId);
    }

    componentWillMount(){
        console.log("Going for avatars...");
        let datos=JSON.parse(localStorage.getItem('activeUsr'));
        localStorage.setItem("avatarSelected", JSON.stringify(datos.Avatar));
        this.state.avatarSelected= datos.Avatar;
        let urlSubmitComment = Router.action('Helper', 'GetAvatars');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("avatars retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ avatars: data });
                }
                else {
                    console.log("Get avatars ");
                }
            }
        }
        xhr.open('get', urlSubmitComment, true);
        xhr.send()
    }

    render() {
        let self = this;
        const images = this.state.avatars.map((v, i) => {
            let path = iisSiteName+"w3images/" + v;
            let styleSelected;
            console.log(0, self.state.avatarSelected);
            if (self.props.createUser)
            {
                //create user avatar selected
                if (this.state.flag && v == self.state.avatarSelected) {
                    styleSelected = { width: '100px', height: '100px', border: "solid" };
                }
                else
                    styleSelected = { width: '100px', height: '100px' };
            }
            else {
                //change my avatar
                if (self.state.avatarSelected && v == self.state.avatarSelected) {
                    if (!self.props.createUser)
                        styleSelected = { width: '100px', height: '100px', border: "solid" };
                    else
                        styleSelected = { width: '100px', height: '100px' };
                }
                else {
                    styleSelected = { width: '100px', height: '100px' };
                }
            }
            return(
                <figure key={v} style={{ margin: "10px" }} onClick={this.handleClick.bind(this,v)}>
                    <img src={path} alt={v} style={styleSelected} />
                </figure>
            )
        });
        
        const style = {
            height: "120px",
            width: "275px",
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
            <div style={style}>
                {images}
            </div>
        );
    }
}