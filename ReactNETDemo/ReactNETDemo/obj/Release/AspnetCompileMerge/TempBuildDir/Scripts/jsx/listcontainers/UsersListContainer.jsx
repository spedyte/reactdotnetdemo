﻿class UsersList extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={users:[]};
    }

    componentWillMount() {
        let urlSubmitComment = Router.action('User', 'GetSllUsers');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("ListUsers:: gather users...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ users: data });
                }
                else {
                    console.log("ListUsers:: error hathering users...");
                }
            }
        }
        xhr.open('get', urlSubmitComment, true);
        xhr.send()
    }

    render(){

        const usersContentList = this.state.users.map((v, i) => {

            if(i%2==0)
                return(
                <tr key={i}>
                        <td><i className="fa fa-user w3-text-blue w3-large"></i></td>
                        <td>{v.Name+' '+v.LastName+' '+v.SureName}</td>
                        <td><i>{v.Position.Name}</i></td>
                        <td><i>{v.Company.Name}</i></td>
                    </tr>
                );
        else

            return(
                <tr key={i}>
                        <td><i className="fa fa-user w3-text-green w3-large"></i></td>
                         <td>{v.Name+' '+v.LastName+' '+v.SureName}</td>
                        <td><i>{v.Position.Name}</i></td>
                        <td><i>{v.Company.Name}</i></td>
                    </tr>
                );
        });



        return(<tbody>
                  <tr><th></th><th>Name</th><th>Position</th><th>Company</th></tr>
                  {usersContentList}            
               </tbody>);
    }
}


class UsersListContainer extends React.Component {
    render() {
        /*var styleComponent = {
            padding_top: "22px"
        };*/
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="userListContainer">
                    <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable ">
                            <UsersList />
                       </table>
                   </div>

                </div>
               );
    }
}