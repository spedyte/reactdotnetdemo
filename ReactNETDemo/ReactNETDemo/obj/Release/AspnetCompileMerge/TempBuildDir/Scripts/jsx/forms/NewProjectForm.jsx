﻿class NewProjectForm extends React.Component{
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.state = {
            name: '',
            aka: '',
            startDate: '',
            endDate: '',
            systemappId: 0,
            clave:''
        };

        this.handleClaveChange = this.handleClaveChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAkaChange = this.handleAkaChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleSystemSelection = this.handleSystemSelection.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleClaveChange(e) {
        this.setState({ clave: e.target.value });
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleAkaChange(e) {
        this.setState({ aka: e.target.value });
    }

    handleStartDateChange(e) {
        this.setState({ startDate: e.target.value });
    }

    handleEndDateChange(e) {
        this.setState({ endDate: e.target.value });
    }

    handleSystemSelection(systemSelected) {
        this.setState({ systemappId: systemSelected });
    }


    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleSubmit(e) {
        e.preventDefault();
        if (handleValidationsNProject(this.state)) {
            // send request to the server

            this.props.onProjectSubmit({
                Name: this.state.name,
                Clave: this.state.clave,
                Aka: this.state.aka,
                StartDate: this.state.startDate,
                EndDate: this.state.endDate,
                SystemAppId: this.state.systemappId
            });
            this.setState({
                name: '',
                aka: '',
                startDate: '',
                endDate: '',
                systemappId: 0,
                clave: ''
            });
        }
        else {
            return;
        }
    }

    render() {

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, fill the new Project information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td  style={{width:"175px"}}></td>
                        <td>
                            <strong>Name :</strong>
                        </td>
                        <td>
                            <textarea value={this.state.name} placeholder="Name" onChange={this.handleNameChange} cols="50" rows="2"/>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Clave :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.clave} placeholder="WOxxxxxxx" onChange={this.handleClaveChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Short Name:</strong>
                        </td>
                        <td>
                            <textarea  value={this.state.aka} placeholder="Short name of the project" onChange={this.handleAkaChange} cols="50" rows="1"/>
                        </td>
                    </tr>
                     <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Start Date:</strong>
                        </td>
                        <td>
                            <input type="date" value={this.state.startDate} placeholder="Start date" onChange={this.handleStartDateChange} cols="50" rows="3" />
                        </td>
                     </tr>
                     <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>End Date:</strong>
                        </td>
                        <td>
                            <input type="date" value={this.state.endDate} placeholder="End date" onChange={this.handleEndDateChange} cols="50" rows="3" />
                        </td>
                     </tr>
                    <tr>
                         <td style={{width:"175px"}}></td>
                        <td>
                           <strong>Select the System:</strong>
                        </td>
                        <td>
                            <SystemSelector systemSelected={this.handleSystemSelection} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td></td>
                        <td>
                            <input type="submit" style={{width:"175px"}} value="Create Project" />
                        </td>
                    </tr>
                </tbody>
            </table>
          </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"305px"}}></td>
                    <td>
                        <input type="button" style={{width:"175px"}} value="Cancel" onClick={this.handleCancel} />
                    </td>
                </tr>
            </tbody>
        </table>
            </div>

      );
            }
}

function handleValidationsNProject(data) {
    let name = data.name.trim();
    let clave = data.clave.trim();
    let aka = data.aka.trim();
    let startdate = data.startDate.trim();
    let enddate = data.endDate.trim();
    let systeappid = data.systemappId;

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!name && name.length < 2 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Name</strong> not NULL, use only alpha characters and at least 3 in NAME</li>';
        flags[0] = false;
    }
    console.log(0, "name validated");
    if (!clave && clave.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Clave</strong> not NULL, use only alpha characters and at least 3 in CLAVE</li>';
        flags[1] = false;
    }
    console.log(1, "clave validated");
    if (!aka && aka.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Aka</strong> not NULL, use only alpha characters and at least 3 in AKA</li>';
        flags[2] = false;
    }
    console.log(2, "aka validated");
    if (systeappid == 0) {
        valdiationMsg = valdiationMsg + '<li><strong>System</strong> not NULL, select a SYSTEM</li>';
        flags[3] = false;
    }
    console.log(3, "system validated");

    if (!startdate && startdate.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Start Date</strong> not NULL, give a START DATE</li>';
        flags[4] = false;
    }
    console.log(4, "start date validated");

    if (!enddate && enddate.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>End Date</strong> not NULL, give a END DATE</li>';
        flags[5] = false;
    }
    console.log(5, "end date validated");


    if ((new Date(startdate).getTime() === new Date(enddate).getTime()) || (new Date(startdate).getTime() > new Date(enddate).getTime()))
    {
        valdiationMsg = valdiationMsg + '<li><strong>Start Date</strong> most be before to <strong>End Date</strong> </li>';
        flags[6] = false;
    }
    console.log(6, "dates are invalid");
   
    if ((flags[0] && flags[1]) && (flags[2] && flags[3]) && (flags[4] && flags[5]) &&(flags[6]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check PROJECT information", valdiationMsg, "error");
        return false;
    }

}


class NewProjectFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleProjectSubmit = this.handleProjectSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("New project:: Cancel operation...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleProjectSubmit(project) {
        console.log(111, project);
        let data = new FormData();
        data.append('Name', project.Name);
        data.append('Clave', project.Clave);
        data.append('Aka', project.Aka);
        data.append('SystemAppId', project.SystemAppId);
        data.append('StartDate', project.StartDate);
        data.append('EndDate', project.EndDate);

       
        let urlSubmit = Router.action('Project', 'AddNewProject');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            waitingDialog.hide();
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("New project:: gathered from the server...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showSuccessMessage("Done!", "project was added!", 2000);
                }
                else {
                    console.log("New project:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the project.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewProjectForm onProjectSubmit={this.handleProjectSubmit} onCancelAction={this.handleCancelUpdate} />
          </div>
      );
            }
}

