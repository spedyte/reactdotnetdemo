﻿class UserNoteSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = { usersnotes: [], usernoteSelected: 0 };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) 
    {
        this.setState({ usernoteSelected: e.target.value });
        this.props.userNoteSelected(e.target.value);
    }

    componentWillMount(){
        console.log("userNotes :: Going for users...");
        let data = new FormData();
        data.append('userAppId', JSON.parse(localStorage.getItem('activeUsr')).UserAppId);

        let urlRequest = Router.action('User', 'GetAllUserNotes');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("userNotes::users retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ usersnotes: data });
                }
                else {
                    console.log("userNotes:: Something was wrong...");
                }
            }
        }
        xhr.open('post', urlRequest, true);
        xhr.send(data)
    }

    render() {
        let self = this;
        let usersnotes=[<option key="-1" value="0">- Choose a User -</option>];
        let usersnotesDB = this.state.usersnotes.map((v, i) => {
            return(
                <option key={i} value={v.UserAppId}>{v.Name} {v.LastName} {v.SureName}</option>
            )
                });
        usersnotes = usersnotes.concat(usersnotesDB);

                    return(
                            <select onChange={this.handleChange} style={{ width: "175px" }} value={this.state.usernoteSelected}>
                            {usersnotes}
                            </select>
                            );
                            }
}