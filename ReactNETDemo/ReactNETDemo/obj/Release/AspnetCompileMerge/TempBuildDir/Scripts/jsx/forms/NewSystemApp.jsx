﻿class NewSystemForm extends React.Component{
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        console.log("setup1",userSession.Avatar);
        this.state = {
            name: '',
            clave: '',
            description: ''
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleClaveChange = this.handleClaveChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleClaveChange(e) {
        this.setState({ clave: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }


    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleSubmit(e) {
        e.preventDefault();
        if (handleValidations(this.state)) {
            // send request to the server

            this.props.onSystemSubmit({
                Name: this.state.name,
                Clave: this.state.clave,
                Description: this.state.description
            });
            this.setState({
                name: '',
                clave: '',
                description: ''
            });
        }
        else {
            return;
        }
    }

    render() {

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, fill the new System information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td  style={{width:"175px"}}></td>
                        <td>
                            <strong>Name :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.name} placeholder="Name" onChange={this.handleNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Clave :</strong>
                        </td>
                        <td>
                            <input type="text" value={this.state.clave} placeholder="Clave" onChange={this.handleClaveChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Description:</strong>
                        </td>
                        <td>
                            <textarea  value={this.state.description} placeholder="Description" onChange={this.handleDescriptionChange} cols="50" rows="3"/>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td></td>
                        <td>
                            <input type="submit" style={{width:"175px"}} value="Create System" />
                        </td>
                    </tr>
                </tbody>
            </table>
          </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"260px"}}></td>
                    <td>
                        <input type="button" style={{width:"175px"}} value="Cancel" onClick={this.handleCancel} />
                    </td>
                </tr>
            </tbody>
        </table>
            </div>

      );
            }
}

function handleValidations(data) {
    let name = data.name.trim();
    let clave = data.clave.trim();
    let description = data.description.trim();

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!name && name.length < 2 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Name</strong> not NULL, use only alpha characters and at least 3 in NAME</li>';
        flags[0] = false;
    }
    console.log(0, "name validated");
    if (!clave && clave.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Clave</strong> not NULL, use only alpha characters and at least 3 in CLAVE</li>';
        flags[1] = false;
    }
    console.log(1, "clave validated");
    if (!description && description.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>Description</strong> not NULL, use only alpha characters and at least 3 in DESCRIPTION</li>';
        flags[2] = false;
    }
    console.log(2, "description validated");
   
    if ((flags[0] && flags[1]) && (flags[2]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check SYSTEM information", valdiationMsg, "error");
        return false;
    }

}


class NewSystemFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleSystemSubmit = this.handleSystemSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("New System:: Cancel operation...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleSystemSubmit(systemapp) {
        let data = new FormData();
        data.append('Name', systemapp.Name);
        data.append('Clave', systemapp.Clave);
        data.append('Description', systemapp.Description);

        console.log(111, data);
        let urlSubmit = Router.action('SystemApp', 'AddNewSystem');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                waitingDialog.hide();
                if (xhr.responseText) {
                    console.log("New system:: gathered from the server...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showSuccessMessage("Done!", "System was added!", 2000);
                }
                else {
                    console.log("New system:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the system.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewSystemForm onSystemSubmit={this.handleSystemSubmit} onCancelAction={this.handleCancelUpdate} />
          </div>
      );
            }
}

