﻿class SystemSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = { systems:[],systemSelected:0};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) 
    {
        this.setState({ systemSelected: e.target.value });
        this.props.systemSelected(e.target.value);
    }

    componentWillMount(){
        console.log("Systems :: Going for systems...");
        
        let urlRequest = Router.action('SystemApp', 'GetAllSystems');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Systems::systems retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ systems: data });
                }
                else {
                    console.log("Systems:: Something was wrong...");
                }
            }
        }
        xhr.open('get', urlRequest, true);
        xhr.send()
    }

    render() {
        let self = this;
        let systemsapp=[<option key="-1" value="0">- Choose a System -</option>];
        let systemsappDB = this.state.systems.map((v, i) => {
            return(
                <option key={i} value={v.SystemAppId}>{v.Name}</option>
            )
                });
        systemsapp = systemsapp.concat(systemsappDB);

                    return(
                            <select onChange={this.handleChange} style={{ width: "175px" }} value={this.state.systemSelected}>
                            {systemsapp}
                            </select>
                            );
                            }
}