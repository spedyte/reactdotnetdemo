﻿class ReleaseList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = { releases: [] };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(value) {
        localStorage.setItem("releaseSelected", JSON.stringify(value));
        showMessageHTMLFormated("Release selected", "<strong>" + value.Month + " - " + value.Year + "</strong>", "success");
        ReactDOM.render(<DeliveriesListContainer height={"300px"} width={"800px" } />, document.getElementById('deliveriesManager'));
        ReactDOM.render(<CreatMilestonesFormBox />, document.getElementById('createmileStonesDiv'));
    }

    componentWillMount() {
        let urlSubmit = Router.action('Release', 'GetAllReleases');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("ListReleases:: gather releases...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ releases: data });
                }
                else {
                    console.log("ListReleases:: error hathering releases...");
                }
            }
        }
        xhr.open('get', urlSubmit, true);
        xhr.send()
    }

    render(){

        const releasesContentList = this.state.releases.map((v, i) => {

            if(i%2==0)
                return(
                <tr key={i}  onClick={this.handleClick.bind(this, v)}>
                        <td><i className="fa fa-calendar w3-text-green w3-large"></i></td>
                        <td>{v.Month}</td>
                        <td><i>{v.Year}</i></td>
                    </tr>
                );
    else

            return(
                <tr key={i} onClick={this.handleClick.bind(this, v)}>
                        <td><i className="fa fa-calendar w3-text-orange w3-large"></i></td>
                         <td>{v.Month}</td>
                        <td><i>{v.Year}</i></td>
                    </tr>
                );
                        });



                        return(<tbody>
                               <tr><th></th><th>Month</th><th>Year</th></tr>
                        {releasesContentList}            
                               </tbody>);
                        }
}


class ReleasesListContainer extends React.Component {
    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="releasesListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable " id="realeasesList">
                            <ReleaseList />
                       </table>
                   </div>

                </div>
               );
    }
}