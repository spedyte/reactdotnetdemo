﻿class NewReleaseForm extends React.Component {
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        console.log("setup1",userSession.Avatar);
        this.state = {
            month: ''
        };

        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleMonthChange(e) {
        this.setState({ month: e.target.value });
    }


    handleSubmit(e) {
        e.preventDefault();
        if (handleValidationsMonth(this.state)) {
            // send request to the server

            this.props.onReleaseSubmit({
                Month: this.state.month
            });
            this.setState({
                month: ''
            });
        }
        else {
            return;
        }
    }

    render() {

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, fill the new Release information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td >
                            <strong>Release date :</strong>
                        </td>
                        <td>
                            <input type="month" value={this.state.month} placeholder="Month" onChange={this.handleMonthChange} />
                        </td>
                        <td>
                            <input type="submit" style={{width:"175px"}} value="Create Release" />
                        </td>
                    </tr>
                </tbody>
            </table>
          </form>
         </div>

      );
            }
}

function handleValidationsMonth(data) {

    console.log(99,data);
    let month = data.month;

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!month && month.length < 2) {
        valdiationMsg = valdiationMsg + '<li>Please <strong>check</strong> your date choosed</li>';
        flags[0] = false;
    }
   
    if ((flags[0]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check RELEASE information", valdiationMsg, "error");
        return false;
    }

}


class NewReleaseFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleReleaseSubmit = this.handleReleaseSubmit.bind(this);
    }


    handleReleaseSubmit(date) {
        let monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        
        var d = new Date(date.Month);
        let data = new FormData();
        data.append('Month', monthNames[(d.getMonth() + 1) % 12]);
        if (((d.getMonth() + 1) % 12)==0)
        {
            let newYear = d.getFullYear();
            newYear = newYear + 1;
            data.append('Year', "" + newYear + "");
        }    
        else
        {
            data.append('Year', "" + d.getFullYear() + "");
        }

        let urlSubmit = Router.action('Release', 'AddNewRelease');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                waitingDialog.hide();
                if (xhr.responseText) {
                    console.log("New release:: gathered from the server...");
                    ReactDOM.render(<div></div>, document.getElementById('realeasesList'));
                    ReactDOM.render(<ReleaseList />, document.getElementById('realeasesList'));
                    showSuccessMessage("Done!", "Release was added!", 2000);
                }
                else {
                    console.log("New release:: Added error...");
                    ReactDOM.render(<div></div>, document.getElementById('realeasesList'));
                    ReactDOM.render(<ReleaseList />, document.getElementById('realeasesList'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the release.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewReleaseForm onReleaseSubmit={this.handleReleaseSubmit}/>
          </div>
      );
            }
}

