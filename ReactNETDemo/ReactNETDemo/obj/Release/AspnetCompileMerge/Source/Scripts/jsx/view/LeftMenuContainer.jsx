﻿    function handleClickNewUserLM() {
        console.log("Showing new user form....3..2..1..0");
        ReactDOM.render(<NewUserFormBox/>, document.getElementById('content'));
        /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }


    function handleClickNewSystemLM() {
        console.log("Showing new system form....3..2..1..0");
        ReactDOM.render(<NewSystemFormBox />, document.getElementById('content'));
        /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }

    
    function handleClickNewProjectLM() {
        console.log("Showing new project form....3..2..1..0");
        ReactDOM.render(<NewProjectFormBox />, document.getElementById('content'));
        /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }

    function handleClickNewTemplatetLM() {
        console.log("Showing new template form....3..2..1..0");
        ReactDOM.render(<NewTemplateFormBox />, document.getElementById('content'));
        /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }

    function handleClickSettingsLM() {
        console.log("Showing setup user form....3..2..1..0");
        ReactDOM.render(<SetupUserFormBox />, document.getElementById('content'));
        /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }


class LeftMenuContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLogged: flagIsLogged };
        this.updateState = this.updateState.bind(this);
    }
    
    updateState() {
        this.setState({ isLogged: flagIsLogged });
    }

    componentDidMount() {
        this.updateState();
        window.setInterval(this.updateState, this.props.pollInterval);
    }

    render() {
        let activeSession = JSON.parse(localStorage.getItem('activeUsr'));
        if (flagIsLogged)
        {
            //review the user rol 
            if (activeSession.Position.Name === "Administrator")
            {
                console.log("CReate menu...Admin");
                return (<div>
                            <a href="javascript:handleClickNewUserLM()" className="w3-bar-item w3-button w3-padding "><i className="fa fa-user-plus fa-fw"></i>  Create User</a>
                            <a href="javascript:handleClickNewSystemLM()" className="w3-bar-item w3-button w3-padding"><i className="fa fa-laptop fa-fw"></i>  Create System</a>
                            <a href="javascript:handleClickNewProjectLM()" className="w3-bar-item w3-button w3-padding"><i className="fa fa-briefcase fa-fw"></i>  Create Project</a>
                            <a href="javascript:handleClickNewTemplatetLM()" className="w3-bar-item w3-button w3-padding"><i className="fa fa-file fa-fw"></i>  Create Template</a>
                            <a href="javascript:handleClickSettingsLM()" className="w3-bar-item w3-button w3-padding"><i className="fa fa-cog fa-fw"></i>  Settings</a><br/><br/>
                        </div>);
            }
            else if(activeSession.Position.Name == 'Manager')
            {
                console.log("CReate menu...Manager");
                return (<div >
                            <ProjectsLeftMenuContainer />
                            <ReleasesLeftMenuContainer />
                            <a href="javascript:handleClickSettingsLM()"className="w3-bar-item w3-button w3-padding"><i className="fa fa-cog fa-fw"></i>  Settings</a><br/><br/>
                        </div>);
            }
            else {
                console.log("CReate menu...normal user");
                return (<div >
                        <a href="#" className="w3-bar-item w3-button w3-padding "><i className="fa fa-users fa-fw"></i>  Covers(Carátulas)</a>
                        <a href="#" className="w3-bar-item w3-button w3-padding"><i className="fa fa-eye fa-fw"></i>  Milestones(Enregables)</a>
                        <a href="#" className="w3-bar-item w3-button w3-padding"><i className="fa fa-users fa-fw"></i>  Projects</a>
                        <a href="javascript:handleClickSettingsLM()"className="w3-bar-item w3-button w3-padding"><i className="fa fa-cog fa-fw"></i>  Settings</a><br/><br/>
                    </div>);
            }
        }
        else
            return (<div></div>);
    }
}

