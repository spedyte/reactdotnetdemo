﻿class TemplateList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = { templates: [], templateSelected: {} };
        this.handleUpdateTemplate=this.handleUpdateTemplate.bind(this);
    }

    handleUpdateTemplate(template) {
        console.log("Showing setup template form....3..2..1..0");
        ReactDOM.render(<UpdateTemplateFormBox template={template} />, document.getElementById('content'));
       /*ReactDOM.render(<div></div>, document.getElementById('generalStatus'));
        ReactDOM.render(<div></div>, document.getElementById('recentnotes'));
        ReactDOM.render(<div></div>, document.getElementById('percentages'));
        ReactDOM.render(<div></div>, document.getElementById('clasified'));*/
    }

    componentWillMount() {
        let urlSubmit = Router.action('Template', 'GetAllTemplates');
            
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
                
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("TemplateProjects:: gather templates...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ templates: data });
                }
                else {
                    console.log("TemplateProjects:: error hathering templates...");
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send()
    }

    render() {
        let templatesContentList;

        if (this.state.templates == null || this.state.templates.length == 0) {
            templatesContentList = () => { return <tr><td colSpan={3 }>Does not exist <strong>template</strong> for this project</td></tr> };
            }
        else

            //alert("pal map size:::: " + this.state.deliveries.lengt);
            templatesContentList = this.state.templates.map((v, i) => {
            
            if (i % 2 == 0) 
            {
                    return (
                        <tr key={i}>
                        <td><i className="fa fa-file-text w3-text-green w3-large"></i></td>
                        <td>{v.Name}</td>
                        <td><i>{v.Path}</i></td>
                        <td><a href="#"><i className="fa fa-pencil w3-text-blue w3-large" onClick={this.handleUpdateTemplate.bind(this,v)}></i></a></td>
                    </tr>);
            }
        else
        {
                return (
                    <tr key={i }>
                    <td><i className="fa fa-file-text w3-text-orange w3-large"></i></td>
                    <td>{v.Name}</td>
                    <td><i>{v.Path}</i></td>
                    <td><a href="#"><i className="fa fa-pencil w3-text-blue w3-large" onClick={this.handleUpdateTemplate.bind(this,v)}></i></a></td>
                    </tr>);
        }

});
        
       

    return(<tbody>
    <tr>
        <th></th>
        <th>Document</th>
        <th>File Name</th>
        <th></th>
    </tr>
    {templatesContentList}            
    </tbody>);
            
    }
}


class TemplatesListContainer extends React.Component {
    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="templatesListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable ">
                            <TemplateList />
                       </table>
                   </div>

                </div>
               );
    }
}