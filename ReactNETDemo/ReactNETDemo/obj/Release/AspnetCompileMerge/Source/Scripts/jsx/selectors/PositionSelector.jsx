﻿class PositionSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = { positions:[],positionSelected:0};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) 
    {
        this.setState({ positionSelected: e.target.value });
        this.props.positionSelected(e.target.value);
    }

    componentWillMount(){
        console.log("Positions :: Going for positions...");
        
        let urlSubmitRequest = Router.action('Position', 'GetAllPositions');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Positions::positions retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ positions: data });
                }
                else {
                    console.log("Positions:: Something was wrong...");
                }
            }
        }
        xhr.open('get', urlSubmitRequest, true);
        xhr.send()
    }

    render() {
        let self = this;
        let positions=[<option key="-1" value="0">- Choose a Rol -</option>];
        let positionsDB = this.state.positions.map((v, i) => {
            return(
                <option key={i} value={v.PositionId}>{v.Name}</option>
            )
           });
        positions = positions.concat(positionsDB);

        return(
                <select onChange={this.handleChange} style={{width:"175px"}} value={this.state.positionSelected}>
                {positions}
                </select>
                );
    }
}