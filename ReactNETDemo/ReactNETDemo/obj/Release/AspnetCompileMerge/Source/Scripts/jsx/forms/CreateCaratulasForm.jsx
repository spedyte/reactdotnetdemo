﻿class CreateCaratulasForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        let projectSelect = JSON.parse(localStorage.getItem('projectSelected'));
        let releaseSelect = JSON.parse(localStorage.getItem('releaseSelected'));

        if (handleValidationsCreateMilestones({
            Project: projectSelect,
            Release: releaseSelect
        })) {
            // send request to the server
            this.props.onCreateSubmit({
                Project: projectSelect,
                Release: releaseSelect
            });
        }
        else {
            return;
        }
    }

    render() {
        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <td style={{width:"235px"}} ></td>
                        <td ><input type="submit" style={{width:"195px"}} value="Press to Create Caratulas" /></td>
                    </tr>
                </tbody>
            </table>
          </form>
         </div>

      );
            }
}

class CreatCaratulasFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleCaratulasSubmit = this.handleCaratulasSubmit.bind(this);
    }


    handleCaratulasSubmit(info) {

      
        swal({
            title: "Do you want to create caratulas for?",
            text: '<strong>' + info.Project.Name + '<strong> <br/> <strong>' + info.Release.Month + ' ' + info.Release.Year + '</strong>',
            type: "warning",
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, create them!",
            closeOnConfirm: false
        },
            function () {
                                
                let data = new FormData();
                data.append('projectId', info.Project.ProjectId);
                data.append('releaseId', info.Release.ReleaseId);

                let urlSubmit = Router.action('Milestone', 'CreateCaratulasMileStone');
                waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = function ()
                {
                    if (this.readyState === this.DONE)
                    {
                        waitingDialog.hide();
                        if (xhr.responseText)
                        {
                            console.log("Milestones created:: gathered from the server...");
                            ReactDOM.render(<div></div>, document.getElementById('releasesManager'));
                            showSuccessMessage("Done!", "caratulas were created!", 2000);
                            ReactDOM.render(<DeliveriesListContainer height={"300px"} width={"800px" } />, document.getElementById('releasesManager'));
                    }
                    else {
                        console.log("Milestones created:: Added error...");
                        showErrorMessage("Error, Try again!", "Something was wrong while we trying create caratulas.", 3500);
                    }
                }
            }
            xhr.open('post', urlSubmit, true);
        xhr.send(data)

    });

    }

render() {
        return (
          <div>
            <CreateCaratulasForm onCreateSubmit={this.handleCaratulasSubmit}/>
      </div>
      );
      }
}

