﻿class ProjectList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={projects:[]};
    }

    componentWillMount() {
        let urlSubmit = Router.action('Project', 'GetAllProjects');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("ListProjects:: gather projects...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ projects: data });
                }
                else {
                    console.log("ListProjects:: error hathering projects...");
                }
            }
        }
        xhr.open('get', urlSubmit, true);
        xhr.send()
    }

    render(){

        const projectsContentList = this.state.projects.map((v, i) => {

            if(i%2==0)
                return(
                <tr key={i}>
                        <td><i className="fa fa-briefcase w3-text-green w3-large"></i></td>
                        <td>{v.Name}</td>
                        <td><i>{v.Clave}</i></td>
                        <td>{v.Sistema.Name}</td>
                    </tr>
                );
    else

            return(
                <tr key={i}>
                        <td><i className="fa fa-briefcase w3-text-orange w3-large"></i></td>
                         <td>{v.Name}</td>
                        <td><i>{v.Clave}</i></td>
                        <td>{v.Sistema.Name}</td>
                    </tr>
                );
                        });



                        return(<tbody>
                               <tr><th></th><th>Name</th><th>Clave</th><th>System</th></tr>
                        {projectsContentList}            
                               </tbody>);
                        }
}


class ProjectsListContainer extends React.Component {
    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="systemListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable ">
                            <ProjectList />
                       </table>
                   </div>

                </div>
               );
    }
}