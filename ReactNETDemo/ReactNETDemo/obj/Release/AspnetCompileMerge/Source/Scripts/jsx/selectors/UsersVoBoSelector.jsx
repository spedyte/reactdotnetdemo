﻿class UserVoBoSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = { usersvobos: [], uservoboSelected: 0 };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) 
    {
        this.setState({ uservoboSelected: e.target.value });
        this.props.userVoBoSelected(e.target.value);
    }

    componentWillMount(){
        console.log("userVoBos :: Going for users...");
        
        let urlRequest = Router.action('User', 'GetAllUserVoBos');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("userVoBos::users retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ usersvobos: data });
                }
                else {
                    console.log("userVoBos:: Something was wrong...");
                }
            }
        }
        xhr.open('get', urlRequest, true);
        xhr.send()
    }

    render() {
        let self = this;
        let usersvobos=[<option key="-1" value="0">- Choose a User -</option>];
        let usersvobosDB = this.state.usersvobos.map((v, i) => {
            return(
                <option key={i} value={v.UserAppId}>{v.Name} {v.LastName} {v.SureName}</option>
            )
                });
                    usersvobos = usersvobos.concat(usersvobosDB);

                    return(
                            <select onChange={this.handleChange} style={{ width: "175px" }} value={this.state.uservoboSelected}>
                            {usersvobos}
                            </select>
                            );
                            }
}