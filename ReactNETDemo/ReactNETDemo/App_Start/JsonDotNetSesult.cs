﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.App_Start
{
    /// <summary>
    /// The object is already serialized by Json.NET, and when you pass it to Json() it gets encoded twice. 
    /// If you must use Json.NET instead of the built in encoder, then the ideal way to handle this
    /// would be to create a custom ActionResult accepts the object and calls Json.net internally to serialize 
    /// the object and return it as an application/json result.
    /// http://stackoverflow.com/questions/7382265/returning-unescaped-json-in-mvc-with-json-net
    /// </summary>
    public class JsonDotNetResult : ActionResult
    {
        private object _obj { get; set; }
        public JsonDotNetResult(object obj)
        {
            _obj = obj;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.AddHeader("content-type", "application/json");
            context.HttpContext.Response.Write(JsonConvert.SerializeObject(_obj
            ,
              Formatting.Indented,
              new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore,
                  ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore }));
        }
    }
}