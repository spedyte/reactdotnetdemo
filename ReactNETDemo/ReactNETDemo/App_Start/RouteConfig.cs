﻿using FollowingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReactNETDemo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //initializeDB();
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapRoute(
                name: "Comments",
                url: "comments",
                defaults: new { controller = "Home", action = "Comments" }
            );

            routes.MapRoute(
                name: "NewComment",
                url: "comments/new",
                defaults: new { controller = "Home", action = "AddComment" }
            );*/

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        public static void initializeDB()
        {
            using (var context = new FollowingContext())
            {
                // Display all Students from the database
                var usersapp = (from s in context.UsersApp
                                where s.Name=="Administrator"
                                orderby s.Name
                                select s).ToList<UserApp>();

                foreach (var user in usersapp)
                {
                    string name = user.Name + " " + user.LastName;
                    Console.WriteLine("ID: {0}, Name: {1}", user.UserAppId, name);
                }
            }
        }

    }
}
