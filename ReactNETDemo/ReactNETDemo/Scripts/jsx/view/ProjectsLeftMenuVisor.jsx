﻿class ProjectLMList extends React.Component {
    constructor(props)
    {
        super(props);
        this.state={projects:[]};
        this.handleClic = this.handleClic.bind(this);
    }

    handleClic(value) {
        if (value != null) {
            ReactDOM.render(<div></div>, document.getElementById('deliveriesManager'));
            localStorage.setItem("projectSelected", JSON.stringify(value));
            showMessageHTMLFormated("Project selected", "<strong>" + value.Name + "</strong>", "success");
            //sleep(1000);
            console.log("project selected...");
            ReactDOM.render(<DeliveriesListContainer height={"300px"} width={"800px" } />, document.getElementById('deliveriesManager'));
            ReactDOM.render(<CreatMilestonesFormBox />, document.getElementById('createmileStonesDiv'));
            
        }
    }

    componentWillMount() {
        let urlSubmit = Router.action('Project', 'GetAllProjects');
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("ListProjects:: gather projects...");
                    var data = JSON.parse(xhr.responseText);
                    self.setState({ projects: data });
                }
                else {
                    console.log("ListProjects:: error hathering projects...");
                }
            }
        }
        xhr.open('get', urlSubmit, true);
        xhr.send()
    }

    render(){
        const projectsContentList = this.state.projects.map((v, i) => {
            return(<li key={i} >
                    <a onClick={this.handleClic.bind(this,v)}>{v.Name}</a>
                </li>);
        });
        console.log(projectsContentList);
        return(
                <div>{projectsContentList}</div>
            );
        }
    }


class ProjectsLeftMenuContainer extends React.Component {
render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
            <div className="dropdown">
                <a id="dLabel" role="button" data-toggle="dropdown" className="w3-bar-item w3-button w3-padding " data-target="#" href="/page.html">
                    <i className="fa fa-briefcase fa-fw"></i> Select a Project <span className="caret"></span>
                </a>
    		    <ul className="dropdown-menu multi-level scrollable-menu" role="menu" aria-labelledby="dropdownMenu">
                    <ProjectLMList/>
    		    </ul>
             </div>
               );
    }
}