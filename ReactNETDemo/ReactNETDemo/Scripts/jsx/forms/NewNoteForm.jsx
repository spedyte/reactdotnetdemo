﻿class NewNoteForm extends React.Component {
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.state = {
            userNoteId: 0,
            userRegisterNoteId: userSession.UserAppId,
            noteContent:''
        };

        this.handleUserNoteIdChange = this.handleUserNoteIdChange.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleContentChange(e) {
        this.setState({ noteContent: e.target.value });
    }

    handleUserNoteIdChange(userSelected) {
        this.setState({ userNoteId: userSelected });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (handleValidationsNNote(this.state)) {
            // send request to the server

            this.props.onNoteSubmit({
                UserNoteId: this.state.userNoteId,
                NoteContent: this.state.noteContent
            });
            this.setState({
                userNoteId: 0,
                noteContent:''
            });

            ReactDOM.render(<UserNoteSelector userNoteSelected={this.handleUserNoteIdChange} />, document.getElementById('tdUserNoteSelector'));
            
    }
        else {
            return;
        }
        }

    render() {

            return (
                <div>
              <form className="commentForm" onSubmit={this.handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <td style={{width:"215px"}}>Select <strong>Note's User </strong> responsable</td>
                        <td id="tdUserNoteSelector">
                            <UserNoteSelector userNoteSelected={this.handleUserNoteIdChange} />
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Content:</strong></td>
                        <td>
                            <textarea cols="100" rows="4" onChange={this.handleContentChange}></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" style={{width:"175px"}} value="Create  Note" /></td>
                    </tr>
                </tbody>
            </table>
          </form>
      </div>

      );
            }
}

function handleValidationsNNote(data) {
    let uservoboid = data.userNoteId;
    let noteContent = data.noteContent.trim();

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (uservoboid==0) {
        valdiationMsg = valdiationMsg + '<li><strong>SELECT</strong> a USER</li>';
        flags[0] = false;
    }

    if (noteContent.length == 0)
    {
        valdiationMsg = valdiationMsg + '<li>Please <strong>WRITE</strong> the note text</li>';
        flags[1] = false;
    }
    
    if ((flags[0] && flags[1]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check SELECT the user", valdiationMsg, "error");
        return false;
    }

}


class NewNoteFormBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: {},milestone: this.props.milestone};
        this.handleNoteSubmit = this.handleNoteSubmit.bind(this);
        this.onCreateNoteHandle = this.onCreateNoteHandle.bind(this);
    }

    onCreateNoteHandle() {
        this.props.onCreateNote();
    }

    handleNoteSubmit(note) {
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        let data = new FormData();
        data.append('mileStoneId', this.props.milestone.MileStoneId);
        data.append('userNoteId', note.UserNoteId);
        data.append('userRegisterId', userSession.UserAppId);
        data.append('releaseID', (JSON.parse(localStorage.getItem('releaseSelected'))).ReleaseId);
        data.append('noteContent', note.NoteContent);

        let urlSubmit = Router.action('Milestone', 'AddNewNote');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });
        let self = this;
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            waitingDialog.hide();
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("New note:: gathered from the server...");
                    ReactDOM.render(<div></div>, document.getElementById('tableNotes'));
                    ReactDOM.render(<NotesList milestone={self.props.milestone }/>, document.getElementById('tableNotes'));
                    self.onCreateNoteHandle();
                    showSuccessMessage("Done!", "Note was added!", 2000);
                }
                else {
                    console.log("New note:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the Note.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewNoteForm onNoteSubmit={this.handleNoteSubmit} />
          </div>
      );
          }
}