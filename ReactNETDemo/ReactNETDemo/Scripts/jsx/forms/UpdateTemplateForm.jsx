﻿class UpdateTemplateForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            templateId: this.props.template.TemplateId,
            name: this.props.template.Name,
            path: this.props.template.Path
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePathChange = this.handlePathChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }


    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handlePathChange(e) {
        this.setState({ path: e.target.value });
    }

    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleSubmit(e) {
        e.preventDefault();
        let name = this.state.name.trim();
        let path = this.state.path.trim();
        if (!name || !path) {
            return;
        }
        // send request to the server

        this.props.onTemplateSubmit({
            TemplateId: this.props.template.TemplateId,
            Name:this.state.name,
            Path:this.state.path
        });
        this.setState({
            TemplateId: this.props.template.TemplateId,
            Name: '',
            Path: ''
        });
    }

    render() {
        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, update Template information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>Document :</strong>
                        </td>
                        <td>
                            <textarea cols="50" rows="2" value={this.state.name} onChange={this.handleNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>File Name :</strong>
                        </td>
                        <td>
                            <textarea cols="50" rows="2" value={this.state.path} onChange={this.handlePathChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>

                        </td>
                        <td>
                            <input type="submit" style={{width:"180px"}} value="Update template" />
                        </td>
                    </tr>
                </tbody>
            </table>  
        </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"175px"}}></td>
                    <td style={{width:"80px"}}></td>
                    <td>
                        <input type="button" style={{width:"180px"}} value="Cancel" onClick={this.handleCancel}/>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
      );
            }
}

class UpdateTemplateFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleTemplateSubmit = this.handleTemplateSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("cancel update template...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleTemplateSubmit(usr) {
        let data = new FormData();
        data.append('Name', usr.Name);
        data.append('Path', usr.Path);
        data.append('TemplateId', usr.TemplateId);

       let urlSubmitComment = Router.action('Template', 'UpdateTemplate');
        console.log(2, urlSubmitComment);

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("template gathered from the server...");
                    showSuccessMessage("Done!", "Update was successfull!", 2000);
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                }
                else {
                    console.log("Update error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying update the template.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmitComment, true);
        xhr.send(data)

    }

    /*componentDidMount() {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    }*/

    render() {
        return (
          <div>
            <UpdateTemplateForm onTemplateSubmit={this.handleTemplateSubmit} onCancelAction={this.handleCancelUpdate} template={this.props.template}/>
          </div>
      );
            }
}

