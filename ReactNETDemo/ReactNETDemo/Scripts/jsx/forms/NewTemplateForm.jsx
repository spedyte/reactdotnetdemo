﻿class NewTemplateForm extends React.Component{
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.state = {
            name: '',
            fileName:''
        };

        this.handleFileNameChange = this.handleFileNameChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);


        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleFileNameChange(e) {
        this.setState({ fileName: e.target.value });
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleCancel(e) {
        this.props.onCancelAction();
    }

    handleSubmit(e) {
        e.preventDefault();
        if (handleValidationsNTemplate(this.state)) {
            // send request to the server

            this.props.onTemplateSubmit({
                Name: this.state.name,
                FileName: this.state.fileName
            });
            this.setState({
                name: '',
                fileName: ''
            });
        }
        else {
            return;
        }
    }

    render() {

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Please, fill the new Template information</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td  style={{width:"175px"}}></td>
                        <td>
                            <strong>Name :</strong>
                        </td>
                        <td>
                            <textarea value={this.state.name} placeholder="Name" onChange={this.handleNameChange} cols="50" rows="2"/>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td>
                            <strong>FileName :</strong>
                        </td>
                        <td>
                            <input type="text" style={{width:"280px"}} value={this.state.clave} placeholder="*.docx" onChange={this.handleFileNameChange} />
                        </td>
                    </tr>
                    <tr>
                        <td style={{width:"175px"}}></td>
                        <td></td>
                        <td>
                            <input type="submit" style={{width:"175px"}} value="Create Template" />
                        </td>
                    </tr>
                </tbody>
            </table>
          </form>
        <table>
            <tbody>
                <tr>
                    <td style={{width:"250px"}}></td>
                    <td>
                        <input type="button" style={{width:"175px"}} value="Cancel" onClick={this.handleCancel} />
                    </td>
                </tr>
            </tbody>
        </table>
            </div>

      );
            }
}

function handleValidationsNTemplate(data) {
    let name = data.name.trim();
    let fileName = data.fileName.trim();

    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!name && name.length < 2 ) {
        valdiationMsg = valdiationMsg + '<li><strong>Name</strong> not NULL, use only alpha characters and at least 3 in NAME</li>';
        flags[0] = false;
    }
    console.log(0, "name validated");
    if (!fileName && fileName.length < 2) {
        valdiationMsg = valdiationMsg + '<li><strong>File Name</strong> not NULL, use only alpha characters and at least 3 in CLAVE</li>';
        flags[1] = false;
    }
    console.log(1, "clave validated");
      
    if ((flags[0] && flags[1]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check TEMPLATE information", valdiationMsg, "error");
        return false;
    }

}


class NewTemplateFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleTemplateSubmit = this.handleTemplateSubmit.bind(this);
        this.handleCancelUpdate = this.handleCancelUpdate.bind(this);
    }


    handleCancelUpdate() {
        console.log("New template:: Cancel operation...");
        ReactDOM.render(<MainContainer />, document.getElementById('content'));
    }

    handleTemplateSubmit(project) {
        let data = new FormData();
        data.append('Name', project.Name);
        data.append('Path', project.FileName);

       
        let urlSubmit = Router.action('Template', 'AddNewTemplate');
        waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            waitingDialog.hide();
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("New template:: gathered from the server...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showSuccessMessage("Done!", "template was added!", 2000);
                }
                else {
                    console.log("New template:: Added error...");
                    ReactDOM.render(<MainContainer />, document.getElementById('content'));
                    showErrorMessage("Error, Try again!", "Something was wrong while we trying add the template.", 3500);
                }
            }
        }
        xhr.open('post', urlSubmit, true);
        xhr.send(data)

    }

    render() {
        return (
          <div>
            <NewTemplateForm onTemplateSubmit={this.handleTemplateSubmit} onCancelAction={this.handleCancelUpdate} />
          </div>
      );
            }
}

