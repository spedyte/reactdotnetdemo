﻿class CreateMilestonesForm extends React.Component {
    constructor(props) {
        super(props);
        let userSession = JSON.parse(localStorage.getItem('activeUsr'));
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        let projectSelect = JSON.parse(localStorage.getItem('projectSelected'));
        let releaseSelect = JSON.parse(localStorage.getItem('releaseSelected'));

        if (handleValidationsCreateMilestones({
            Project: projectSelect,
            Release: releaseSelect
        })) {
            // send request to the server
            this.props.onCreateSubmit({
                Project: projectSelect,
                Release: releaseSelect
            });
        }
        else {
            return;
        }
    }

    render() {
        let monthRelease = "";
        let projectRelease = "";

        if (JSON.parse(localStorage.getItem('releaseSelected')) != null)
            monthRelease = JSON.parse(localStorage.getItem('releaseSelected')).Month + " - " + JSON.parse(localStorage.getItem('releaseSelected')).Year;
        else
            monthRelease="-- Select a Month --"
        if (JSON.parse(localStorage.getItem('projectSelected')) != null)
            projectRelease = JSON.parse(localStorage.getItem('projectSelected')).Name;
        else
            projectRelease = "-- Select a Project -";

        return (
            <div>
          <form className="commentForm" onSubmit={this.handleSubmit}>
            <span><strong>Press to create related Milestones</strong> </span><br /><br />
            <table>
                <tbody>
                    <tr>
                        <td></td>
                        <td><strong> Project:</strong></td>
                        <td>{projectRelease}
                        </td>
                        <td style={{width:"50px"}}></td>
                        <td><input type="submit" style={{width:"195px"}} value="Press to Create MileStones" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><strong> Release:</strong></td>
                        <td>
                            {monthRelease}
                        </td>
                        <td style={{width:"50px"}}></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
          </form>
         </div>

      );
            }
}

function handleValidationsCreateMilestones(data) {
    let projectSelect = (data.Project)?data.Project:null;//JSON.parse(getItem('projectSelected'));
    let releaseSelect = (data.Release)?data.Release:null;//JSON.parse(getItem('releaseSelected'));

   
    let valdiationMsg = '<ul class=&quot;unstyled&quot;>';

    let flags = [true, true, true, true, true, true, true, true,true];

    if (!projectSelect || projectSelect.ProjectId==0) {
        valdiationMsg = valdiationMsg + '<li>Please <strong>select</strong> a PROJECT</li>';
        flags[0] = false;
    }
    if (!releaseSelect || releaseSelect.ReleaseId == 0) {
        valdiationMsg = valdiationMsg + '<li>Please <strong>select</strong> a RELEASE MONTH</li>';
        flags[0] = false;
    }
   
    if ((flags[0] && flags[1]))
    {
        return true;
    }
    else {
        showMessageHTMLFormated("Please, check your information", valdiationMsg, "error");
        return false;
    }

}


class CreatMilestonesFormBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data: {} };
        this.handleReleaseSubmit = this.handleReleaseSubmit.bind(this);
    }


    handleReleaseSubmit(info) {
        let urlSubmit = Router.action('Template', 'GetAllTemplates');
        swal({
                title: "Do you want to create milestones for?",
                text: '<strong>' + info.Project.Name + '<strong> <br/> <strong>' + info.Release.Month + ' ' + info.Release.Year + '</strong>',
                type: "warning",
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, create them!",
                closeOnConfirm: false
            },
            function () {
                                
                let data = new FormData();
                data.append('projectId', info.Project.ProjectId);
                data.append('releaseId', info.Release.ReleaseId);

                let urlSubmit = Router.action('Milestone', 'CreateMilestonestoReleaseProject');
                waitingDialog.show('We are working...please wait', { dialogSize: 'sm', progressType: 'warning' });

                let xhr = new XMLHttpRequest()
                xhr.onreadystatechange = function ()
                {
                    if (this.readyState === this.DONE)
                    {
                        waitingDialog.hide();
                        if (xhr.responseText)
                        {
                            console.log("Milestones created:: gathered from the server...");
                            ReactDOM.render(<DeliveryList />, document.getElementById('deliveriesList'));
                            //ReactDOM.render(<DeliveriesListContainer height={"300px"} width={"800px" } />, document.getElementById('releasesManager'));
                            showSuccessMessage("Done!", "Release was added!", 2000);
                        }
                        else {
                            console.log("Milestones created:: Added error...");
                            showErrorMessage("Error, Try again!", "Something was wrong while we trying create the milestones.", 3500);
                        }
                    }
                }
                xhr.open('post', urlSubmit, true);
                xhr.send(data)

            }
        );

}

render() {
    return (
      <div>
        <CreateMilestonesForm onCreateSubmit={this.handleReleaseSubmit}/>
      </div>
      );
}
}

