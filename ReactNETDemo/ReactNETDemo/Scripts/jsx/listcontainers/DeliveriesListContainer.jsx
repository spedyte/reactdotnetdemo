﻿class DeliveryList extends React.Component {
    constructor(props)
    {
        let projectSession = JSON.parse(localStorage.getItem('projectSelected'));
        let releasetSession = JSON.parse(localStorage.getItem('releaseSelected'));
        
        let idProject,idRelease;
        super(props);
        //alert("session:: "+projectSession);
        if (projectSession)
            idProject = projectSession.ProjectId;
        else
            idProject = 0;
        //alert("idProject::::: "+idProject);

        if (releasetSession)
            idRelease = releasetSession.ReleaseId;
        else
            idRelease = 0;

        this.state = { deliveries: [], projectId: idProject, releaseId: idRelease };

        this.handleAddVoBo=this.handleAddVoBo.bind(this);
        this.handleAddNote=this.handleAddNote.bind(this);
    }

    handleAddVoBo(milestone) {
        ReactDOM.render(<VoBosListContainer milestone={milestone} height={"300px"} width={"800px"} />, document.getElementById('content'));
    }

    handleAddNote(milestone) {
        ReactDOM.render(<NotesListContainer milestone={milestone} height={"300px"} width={"1000px"} />, document.getElementById('content'));
    }

    componentWillMount() {
            let data = new FormData();
            data.append('projectId', this.state.projectId);
            data.append('releaseId', this.state.releaseId);

            let urlSubmit = Router.action('Delivery', 'GetAllDeliveriesByProject');
            
            let self = this;
            let xhr = new XMLHttpRequest()
            xhr.onreadystatechange = function () {
                
                if (this.readyState === this.DONE) {
                    if (xhr.responseText) {
                        console.log("ListProjects:: gather projects...");
                        var data = JSON.parse(xhr.responseText);
                        self.setState({ deliveries: data });
                    }
                    else {
                        console.log("ListProjects:: error hathering projects...");
                    }
                }
            }
            xhr.open('post', urlSubmit, true);
            xhr.send(data)
    }

    render() {
        let deliveriesContentList;

        if (this.state.deliveries== null || this.state.deliveries.length == 0) {
            //alert("longitud 0 jojojo  XXX size:: " + this.state.deliveries.length);
            deliveriesContentList = () => { return <tr><td colSpan={6 }>Does not exist <strong>deliveries</strong> for this project</td></tr> };
        }
        else

            //alert("pal map size:::: " + this.state.deliveries.lengt);
            deliveriesContentList = this.state.deliveries.map((v, i) => {
                let contentDigitalized;
                if (v.isDigitalized)
                    contentDigitalized = () => {return (<i className="fa fa-file-text w3-text-green w3-large"></i>)};
                else
                    contentDigitalized = () => { return (<i className="fa fa-times w3-text-red w3-small"></i>) };

             
                if (i % 2 == 0) 
                {
                    if (v.isDigitalized)
                        return (
                            <tr key={i }>
                            <td><i className="fa fa-file-text w3-text-green w3-large"></i></td>
                            <td>{v.Name}</td>
                            <td><i>{v.Month}</i></td>
                            <td><i>{v.Year}</i></td>
                            <td><i className="fa fa-check w3-text-green w3-small"></i></td>
                            <td>{v.NumVoBos}  <a href="#"><i className="fa fa-thumbs-o-up w3-text-blue w3-small" onClick={this.handleAddVoBo.bind(this,v)}></i></a></td>
                            <td>{v.NumNotes}  <a href="#"><i className="fa fa-commenting w3-text-orange w3-small" onClick={this.handleAddNote.bind(this,v)}></i></a></td>
                        </tr>);
                    else
                        return (
                            <tr key={i }>
                            <td><i className="fa fa-file-text w3-text-green w3-large"></i></td>
                            <td>{v.Name}</td>
                            <td><i>{v.Month}</i></td>
                            <td><i>{v.Year}</i></td>
                            <td><i className="fa fa-times w3-text-red w3-small"></i></td>
                            <td>{v.NumVoBos}  <a href="#"><i className="fa fa-thumbs-o-up w3-text-blue w3-small" onClick={this.handleAddVoBo.bind(this,v)}></i></a></td>
                            <td>{v.NumNotes}  <a href="#"><i className="fa fa-commenting w3-text-orange w3-small" onClick={this.handleAddNote.bind(this,v)}></i></a></td>
                        </tr>);
                }
                else
                {
                    if (v.isDigitalized)
                        return (
                            <tr key={i }>
                            <td><i className="fa fa-file-text w3-text-orange w3-large"></i></td>
                            <td>{v.Name}</td>
                            <td><i>{v.Month}</i></td>
                            <td><i>{v.Year}</i></td>
                            <td><i className="fa fa-check w3-text-green w3-small"></i></td>
                            <td>{v.NumVoBos}  <a href="#"><i className="fa fa-thumbs-o-up w3-text-blue w3-small" onClick={this.handleAddVoBo.bind(this,v)}></i></a></td>
                            <td>{v.NumNotes}  <a href="#"><i className="fa fa-commenting w3-text-orange w3-small" onClick={this.handleAddNote.bind(this,v)}></i></a></td>
                            </tr>);
                    else
                        return (
                            <tr key={i }>
                            <td><i className="fa fa-file-text w3-text-orange w3-large"></i></td>
                            <td>{v.Name}</td>
                            <td><i>{v.Month}</i></td>
                            <td><i>{v.Year}</i></td>
                            <td><i className="fa fa-times w3-text-red w3-small"></i></td>
                            <td>{v.NumVoBos}  <a href="#"><i className="fa fa-thumbs-o-up w3-text-blue w3-small" onClick={this.handleAddVoBo.bind(this,v)}></i></a></td>
                            <td>{v.NumNotes}  <a href="#"><i className="fa fa-commenting w3-text-orange w3-small" onClick={this.handleAddNote.bind(this,v)}></i></a></td>
                            </tr>);

                }

            });
        
       

                return(<tbody>
                <tr>
                    <th></th>
                    <th>Document Name</th>
                    <th>Month</th>
                    <th>Year</th>
                    <th>Scanned</th>
                    <th></th>
                    <th></th>
                </tr>
                    {deliveriesContentList}            
                </tbody>);
            
        }
}


class DeliveriesListContainer extends React.Component {
    render() {
        const style = {
            height: this.props.height,
            width: this.props.width,
            overflow: "auto",
            display: "flex",
            justifiedContent:"center",
            flexWrap: "wrap"
        } 

        return(
             <div className="w3-twothird" id="deliveriesListContainer">
                   <div style={style}>
                       <table className="w3-table w3-striped w3-white w3-bordered w3-border w3-hoverable " id="deliveriesList">
                            <DeliveryList />
                       </table>
                   </div>

                </div>
               );
    }
}