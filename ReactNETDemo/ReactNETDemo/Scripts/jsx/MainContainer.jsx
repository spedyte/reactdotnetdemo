﻿class MainContainer extends React.Component {
    render() {
        let styleComponent = {
            margin:"0 -16px"
        };
        console.log(20, "loading main container...");

        let activeSession = JSON.parse(localStorage.getItem('activeUsr'));

        if (flagIsLogged && activeSession.Position.Name == "Administrator") {
            return (
              <div id="MainContainer">
                    
                    <div id="mainReactContainer">
                        <DashboardHeader />
                    </div>

                   <div className="col-sm-6">
                    <div className="col-xs-3">
                        <ul className="nav nav-tabs tabs-left vertical-text">
                            <li className="active"><a href="#home-v" data-toggle="tab"><strong>Users</strong></a></li>
                            <li><a href="#profile-v" data-toggle="tab"><strong>Systems</strong></a></li>
                            <li><a href="#messages-v" data-toggle="tab"><strong>Projects</strong></a></li>
                            <li><a href="#templates-v" data-toggle="tab"><strong>Templates</strong></a></li>
                        </ul>
                    </div>
                    <div className="col-xs-9">
                        <div className="tab-content">
                            <div className="tab-pane active" id="home-v"><UsersListContainer height={"300px"} width={"800px"} /></div>
                            <div className="tab-pane" id="profile-v"><SystemsListContainer height={"300px"} width={"800px"} /></div>
                            <div className="tab-pane" id="messages-v"><ProjectsListContainer height={"300px"} width={"800px"} /></div>
                            <div className="tab-pane" id="templates-v"><TemplatesListContainer height={"300px"} width={"800px"} /></div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                   </div>


          
            </div>
            );
        }
        else if (flagIsLogged && activeSession.Position.Name == "Manager") {
            return (
              <div id="MainContainer" className="w3-main">
                    
                    <div id="mainReactContainer">
                        <DashboardHeader />
                    </div>
                    <div className="col-sm-6">
                    <div className="col-xs-3">
                        <ul className="nav nav-tabs tabs-left vertical-text">
                            <li ><a href="#releases-v" data-toggle="tab"><strong>Releases</strong></a></li>
                            <li><a href="#milestones-v" data-toggle="tab"><strong>Milestones</strong></a></li>
                            <li><a href="#templates-v" data-toggle="tab"><strong>Templates</strong></a></li>
                        </ul>
                    </div>
                    <div className="col-xs-9">
                        <div className="tab-content">
                            <div className="tab-pane" id="releases-v">
                                <div id="newRelease"><NewReleaseFormBox /></div><br />
                                <div id="releasesManager"><ReleasesListContainer height={"300px"} width={"800px"} /></div>
                            </div>
                            <div className="tab-pane" id="milestones-v">
                                <div id="createmileStonesDiv"><CreatMilestonesFormBox /></div>
                                 <div id="createCaratulasDiv"><CreatCaratulasFormBox /></div><br />
                                <div id="deliveriesManager">
                                    <DeliveriesListContainer height={"300px"} width={"800px"} />
                                </div><br/><p>.</p>
                            </div>
                            <div className="tab-pane" id="templates-v"><TemplatesListContainer height={"300px"} width={"800px"} /></div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    </div>
                  <br/><br />
            </div>
            );
        }
        else{
            return (
              <div id="MainContainer">
                   
                    <div id="mainReactContainer">
                        <DashboardHeader />
                    </div>
                    <div className="col-sm-6">
                    <div className="col-xs-3">
                        <ul className="nav nav-tabs tabs-left vertical-text">
                            <li className="active"><a href="#home-v" data-toggle="tab"><strong>Projects</strong></a></li>
                            <li><a href="#profile-v" data-toggle="tab"><strong>Deliveries</strong></a></li>
                            <li><a href="#messages-v" data-toggle="tab"><strong>Templates</strong></a></li>
                        </ul>
                    </div>
                    <div className="col-xs-9">
                        <div className="tab-content">
                            <div className="tab-pane active" id="home-v"><ProjectsListContainer height={"300px"} width={"800px"} /></div>
                            <div className="tab-pane" id="profile-v"><SystemsListContainer height={"300px"} width={"800px"} /></div>
                            <div className="tab-pane" id="messages-v"><ProjectsListContainer height={"300px"} width={"800px"} /></div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    </div>
            </div>
            );
        }
    }
}
