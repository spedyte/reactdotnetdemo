﻿class CompanySelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = { companies:[],companySelected:0};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) 
    {
        this.setState({companySelected: e.target.value});
        this.props.companySelected(e.target.value);
    }

    componentWillMount(){
        console.log("Companies :: Going for companies...");
        
        let urlSubmitRequest = Router.action('Company', 'GetAllCompanies');
        let xhr = new XMLHttpRequest()
        let self= this;
        xhr.onreadystatechange = function () {
            if (this.readyState === this.DONE) {
                if (xhr.responseText) {
                    console.log("Companies::companies retrieved...");
                    var data=JSON.parse(xhr.responseText);
                    self.setState({ companies: data });
                }
                else {
                    console.log("Companies:: Something was wrong...");
                }
            }
        }
        xhr.open('get', urlSubmitRequest, true);
        xhr.send()
    }

    render() {
        let self = this;
        let companies=[<option key="-1" value="0">- Choose a Company -</option>];
        let companiesDB=this.state.companies.map((v, i) => {
            return(
                <option key={i} value={v.CompanyId}>{v.Name}</option>
            )
        });
        companies= companies.concat(companiesDB);

    return(
            <select onChange={this.handleChange} style={{width:"175px"}} value={this.state.companySelected}>
                {companies}
            </select>
            );
}
}