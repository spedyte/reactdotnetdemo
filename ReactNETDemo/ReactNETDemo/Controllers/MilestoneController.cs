﻿using BussinesProject;
using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class MilestoneController : Controller
    {
        // GET: Milestone
        public ActionResult Index()
        {
            return View();
        }

        Dictionary<string, int> myMonths = new Dictionary<string, int>() { {"Enero",1}, { "Febrero", 2 }, { "Marzo", 3 }, { "Abril", 4 }, { "Mayo", 5 }, { "Junio", 6 }, { "Julio", 7 },
        {"Agosto",8},{"Septiembre",9},{"Octubre",10},{"Noviembre",11},{"Diciembre",12}};

        [HttpPost]
        public ActionResult CreateMilestonestoReleaseProject(int projectId, int releaseID)
        {
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var templates= (from p in context.Templates
                                orderby p.TemplateId
                                select p).ToList();
                Release release = (from r in context.Releases
                                   where r.ReleaseId == releaseID
                                   select r).SingleOrDefault();

                foreach (var template in templates)
                {
                    MileStone milestone = new MileStone() { Name = template.Name, isDigitalized = false,isOpened=true, Month=release.Month,
                                                            NumVoBos =0,ProjectId=projectId,TemplateId=template.TemplateId, Year=release.Year };
                    context.MileStones.Add(milestone);
                    context.SaveChanges();
                }
            }

            return new JsonDotNetResult(projectId);
        }

        [HttpPost]
        public ActionResult CreateCaratulasMileStone(int projectId, int releaseID)
        {
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var templates = (from p in context.Templates
                                 orderby p.TemplateId
                                 select p).ToList();
                Release release = (from r in context.Releases
                                   where r.ReleaseId == releaseID
                                   select r).SingleOrDefault();

                Project project = (from r in context.Projects.Include("Sistema")
                                   where r.ProjectId == projectId
                                   select r).SingleOrDefault();

                DocxOperations docxOperator = new DocxOperations();
                DirectoryInfo directory = new DirectoryInfo(Server.MapPath(@"~\Templates"));
                DirectoryInfo directoryOutput = new DirectoryInfo(Server.MapPath(@"~\documents"));

                var pathTemplates = directory.FullName;
                var pathDocuments= directoryOutput.FullName;

                SettingFileReader sfr = new SettingFileReader();
                IEnumerable<string[]> keys = sfr.ReadTXTSettingFile(Server.MapPath("."), "templatePatern.txt");

                Dictionary<string, string> values = new Dictionary<string, string>();
                foreach (var keyPatern in keys)
                {
                    //if 
                    values.Add(keyPatern[0], project.Clave);
                    values.Add("%PROJECT_NAME%", project.Name);
                    values.Add("%SYSTEM%", project.Sistema.Clave.ToUpper());
                    values.Add("%MONTH%", release.Month.ToUpper());
                    values.Add("%DATE%", getCaratulaDate(release.Month, release.Year));
                }
                /*values.Add("%PROJECT_CVE%", project.Clave);
                values.Add("%PROJECT_NAME%", project.Name);
                values.Add("%SYSTEM%", project.Sistema.Clave.ToUpper());
                values.Add("%MONTH%", release.Month.ToUpper());
                values.Add("%DATE%", getCaratulaDate(release.Month, release.Year));*/


                // Determine whether the directory exists.
                if (!Directory.Exists((@pathDocuments + "\\" + project.Sistema.Name + "\\" + project.Name + "\\" + release.Year + "\\" + release.Month)))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(@pathDocuments + "\\" + project.Sistema.Name + "\\" + project.Name + "\\" + release.Year + "\\" + release.Month);
                }
                
                foreach (var template in templates)
                {
                    //load template
                    docxOperator.CreateCaratulas(@pathDocuments+"\\"+project.Sistema.Name+"\\"+project.Name+"\\"+release.Year+"\\"+release.Month+"\\"+template.Path,
                        @pathTemplates +"\\" + template.Path,
                        values);
                }
            }

            return new JsonDotNetResult(projectId);
        }

        private string getCaratulaDate(string month,string year)
        {
            int numberMonth = myMonths[month];
            int numberYear = int.Parse(year);

            int firstWorkDay= int.Parse(generalDataResources.firstWorkDay);

            DateTime date = new DateTime(numberYear, numberMonth, firstWorkDay);
            date = date.AddMonths(1);

            if ((date.DayOfWeek == DayOfWeek.Saturday))
            {
                date = date.AddDays(2);
                //date = new DateTime(numberYear, numberMonth, 2);
            }
            else if ((date.DayOfWeek == DayOfWeek.Sunday))
            {
                date = date.AddDays(1);
                //date = new DateTime(numberYear, numberMonth, 1);
            }

            return date.ToString("dd/MM/yyyy");
        }



        [HttpPost]
        public ActionResult GetVoBos(MileStone milestone)
        {
            IList<VoBo> vobos = new List<VoBo>();

            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                vobos= (from p in context.VoBos.Include("UserVoBo").Include("UserRegister")
                                 orderby p.VoBoId
                        where p.MilestoneId == milestone.MileStoneId
                                 select p).ToList();
            }

            return new JsonDotNetResult(vobos);
        }

        [HttpPost]
        public ActionResult GetNotes(MileStone milestone)
        {
            IList<Note> notes = new List<Note>();

            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                notes = (from p in context.Notes.Include("UserNoteResp").Include("UserNoteReg")
                         orderby p.NoteId
                         where p.MilestoneId == milestone.MileStoneId
                         select p).ToList();
            }

            return new JsonDotNetResult(notes);
        }


        [HttpPost]
        public ActionResult AddNewVoBo(int mileStoneId, int userVoBoId, int userRegisterId,int releaseID)
        {
            VoBo vobo = null;
            try
            {
                
                using (var context = new FollowingContext())
                {
                    context.Configuration.ProxyCreationEnabled = false;
                
                    Release release = (from r in context.Releases
                                       where r.ReleaseId == releaseID
                                       select r).SingleOrDefault();

                    UserApp userVobo = (from u in context.UsersApp
                                        where u.UserAppId == userVoBoId
                                        select u).SingleOrDefault();

                    UserApp userReg = (from u in context.UsersApp
                                        where u.UserAppId == userRegisterId
                                       select u).SingleOrDefault();

                    MileStone milestone = (from u in context.MileStones
                                       where u.MileStoneId == mileStoneId
                                           select u).SingleOrDefault();


                    vobo = new VoBo() {Year=release.Year, Month=release.Month, DateVoboReigster= DateTime.Today, MilestoneId = mileStoneId,
                        UserVoBoNickname = userVobo.NickName, UserRegisterNickname= userReg.NickName,ProjectId= milestone.ProjectId,TemplateId = milestone.TemplateId};

                    context.VoBos.Add(vobo);

                    using (var dbContextTransaction = context.Database.BeginTransaction())
                        { 
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
               
                }
                using (var contextDos = new FollowingContext())
                {
                    MileStone milestone = (from u in contextDos.MileStones
                                           where u.MileStoneId == mileStoneId
                                           select u).SingleOrDefault();
                    using (var dbContextTransaction = contextDos.Database.BeginTransaction())
                    {
                        contextDos.Entry(milestone).State = System.Data.Entity.EntityState.Modified;
                        int numVobos = (from p in contextDos.VoBos
                                        orderby p.VoBoId
                                        where p.MilestoneId == mileStoneId
                                        select p).ToList().Count;

                        milestone.NumVoBos = numVobos;
                        contextDos.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                }

                }catch (Exception ex)
            {

                string msg = ex.ToString();
            }
            return new JsonDotNetResult(vobo);
        }


        [HttpPost]
        public ActionResult AddNewNote(int mileStoneId, int userNoteId, int userRegisterId, int releaseID,string noteContent)
        {
            Note note = null;
            try
            {

                using (var context = new FollowingContext())
                {
                    context.Configuration.ProxyCreationEnabled = false;

                    Release release = (from r in context.Releases
                                       where r.ReleaseId == releaseID
                                       select r).SingleOrDefault();

                    UserApp userNote = (from u in context.UsersApp
                                        where u.UserAppId == userNoteId
                                        select u).SingleOrDefault();

                    UserApp userReg = (from u in context.UsersApp
                                       where u.UserAppId == userRegisterId
                                       select u).SingleOrDefault();

                    MileStone milestone = (from u in context.MileStones
                                           where u.MileStoneId == mileStoneId
                                           select u).SingleOrDefault();


                    note = new Note()
                    {
                        Year = release.Year,
                        Month = release.Month,
                        DateNoteReigster = DateTime.Today,
                        MilestoneId = mileStoneId,
                        UserNoteRespNickname = userNote.NickName,
                        UserNoteRegNickname = userReg.NickName,
                        ProjectId = milestone.ProjectId,
                        TemplateId = milestone.TemplateId,
                        Content = noteContent,
                        IsAttended = false
                    };

                    context.Notes.Add(note);

                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }

                }
                using (var contextDos = new FollowingContext())
                {
                    MileStone milestone = (from u in contextDos.MileStones
                                           where u.MileStoneId == mileStoneId
                                           select u).SingleOrDefault();
                    using (var dbContextTransaction = contextDos.Database.BeginTransaction())
                    {
                        contextDos.Entry(milestone).State = System.Data.Entity.EntityState.Modified;
                        int numNotes = (from p in contextDos.Notes
                                        orderby p.NoteId
                                        where p.MilestoneId == mileStoneId
                                        select p).ToList().Count;

                        milestone.NumNotes = numNotes;
                        contextDos.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                }

            }
            catch (Exception ex)
            {

                string msg = ex.ToString();
            }
            return new JsonDotNetResult(note);
        }

    }
}