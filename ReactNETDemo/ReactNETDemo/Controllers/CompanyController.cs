﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action to retrieve all the companies availables for the administrator
        /// to create new users
        /// </summary>
        /// <returns>Json Object List with the information of the companies</returns>
        public ActionResult GetAllCompanies()
        {
            IList<Company> companies = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                companies = (from c in context.Companies
                          orderby c.CompanyId
                          select c).ToList();
            }

            //Parse the object to Json
            return new JsonDotNetResult(companies);
        }
    }
}