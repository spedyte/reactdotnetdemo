﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class NoteController : Controller
    {
        // GET: Note
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult MarkNoteAsAttended(int noteId)
        {
            Note note = null;
            using (var context = new FollowingContext())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    context.Configuration.ProxyCreationEnabled = false;
                    note = (from s in context.Notes
                            where s.NoteId == noteId
                            select s).SingleOrDefault();

                    note.IsAttended = true;
                    context.SaveChanges();
                    dbContextTransaction.Commit();
                }
            }

            return new JsonDotNetResult(note);
        }
    }
}