﻿using FollowingModel;
using Newtonsoft.Json;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ReactNETDemo.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetUser(UserApp user)
        {
            UserApp userDB = null;
            //UserAppPOJO userBean = null;

            user.Pwd = Security.SecurityHelper.Encrypt(user.Pwd);
            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                userDB = (from s in context.UsersApp.Include(p => p.Position).Include(c => c.Company)
                          where s.NickName.Equals(user.NickName) && s.Pwd.Equals(user.Pwd)
                          select s).SingleOrDefault();
            }
            if (userDB != null)
            {
                userDB.Pwd = Security.SecurityHelper.Decrypt(userDB.Pwd);
            }

            //Parse the object to Json
            return new JsonDotNetResult(userDB);
        }

        [HttpPost]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult UpdateUser(UserApp user, string newPwd)
        {
            UserApp userDB = null;
            //UserAppPOJO userBean = null;
            user.Pwd = Security.SecurityHelper.Encrypt(user.Pwd);
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                userDB = (from s in context.UsersApp.Include(p => p.Position).Include(c => c.Company)
                          where s.NickName.Equals(user.NickName) && s.Pwd.Equals(user.Pwd)
                          select s).SingleOrDefault();

                userDB.Name = user.Name;
                userDB.LastName = user.LastName;
                userDB.SureName = user.SureName;
                userDB.NickName = user.NickName;
                userDB.Email = user.Email;
                userDB.Avatar = user.Avatar;
                if (!string.IsNullOrEmpty(newPwd))
                    userDB.Pwd = Security.SecurityHelper.Encrypt(newPwd);
                else
                    userDB.Pwd = user.Pwd;
                context.SaveChanges();
            }
            if (userDB != null)
            {
                userDB.Pwd = Security.SecurityHelper.Decrypt(userDB.Pwd);
            }

            return new JsonDotNetResult(userDB);
            //return Json(userBean, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddNewUser(UserApp user)
        {
            user.Pwd = Security.SecurityHelper.Encrypt(user.Pwd);
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.UsersApp.Add(user);
                context.SaveChanges();
            }

            return new JsonDotNetResult(user);
        }

        //[HttpPost]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetSllUsers()
        {
            IList<UserApp> users = null;
            
            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                users = (from s in context.UsersApp.Include(p => p.Position).Include(c => c.Company)
                         orderby s.UserAppId
                          select s).ToList();
            }
            //Parse the object to Json
            return new JsonDotNetResult(users);
        }


        public ActionResult GetAllUserVoBos()
        {
            IList<UserApp> users = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                users = (from s in context.UsersApp.Include(c => c.Company)
                         where s.Company.Name!="Ultrasist"
                         orderby s.UserAppId
                         select s).ToList();
            }
            //Parse the object to Json
            return new JsonDotNetResult(users);
        }

        public ActionResult GetAllUserNotes(int userAppId)
        {
            IList<UserApp> users = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                users = (from s in context.UsersApp.Include(c => c.Company).Include(p=> p.Position)
                         where s.Company.Name == "Ultrasist"
                            && s.UserAppId!= userAppId  && s.Position.Name!="Administrator"
                         orderby s.UserAppId
                         select s).ToList();
            }
            //Parse the object to Json
            return new JsonDotNetResult(users);
        }


    }
}