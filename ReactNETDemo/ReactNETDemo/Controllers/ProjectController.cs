﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Project
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllProjects()
        {
            IList<Project> projects= null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                projects = (from p in context.Projects.Include("Sistema")
                            orderby p.ProjectId
                         select p).ToList();

            }
            //Parse the object to Json
            return new JsonDotNetResult(projects);
        }

        [HttpPost]
        public ActionResult AddNewProject(Project project)
        {

            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Projects.Add(project);
                context.SaveChanges();
            }

            return new JsonDotNetResult(project);
        }
    }
}