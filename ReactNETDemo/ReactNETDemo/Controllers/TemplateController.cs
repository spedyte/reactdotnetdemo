﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class TemplateController : Controller
    {
        // GET: Template
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllTemplates()
        {
            IList<Template> templates = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                templates = (from p in context.Templates
                            orderby p.TemplateId
                            select p).ToList();

            }
            //Parse the object to Json
            return new JsonDotNetResult(templates);
        }

        [HttpPost]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult UpdateTemplate(Template template)
        {
            Template templateDB = new Template(); ;
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                templateDB = (from s in context.Templates
                          where s.TemplateId==template.TemplateId
                          select s).SingleOrDefault();
                templateDB.Name = template.Name;
                templateDB.Path = template.Path;
                context.SaveChanges();
            }

            return new JsonDotNetResult(templateDB);
        }

        [HttpPost]
        public ActionResult AddNewTemplate(Template template)
        {

            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Templates.Add(template);
                context.SaveChanges();
            }

            return new JsonDotNetResult(template);
        }
    }
}