﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class SystemAppController : Controller
    {
        // GET: SystemApp
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllSystems()
        {
            IList<SystemApp> systems = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                systems = (from s in context.SystemsApp
                           orderby s.SystemAppId
                            select s).ToList();
            }
            //Parse the object to Json
            return new JsonDotNetResult(systems);
        }

        [HttpPost]
        public ActionResult AddNewSystem(SystemApp sytemapp)
        {
            
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.SystemsApp.Add(sytemapp);
                context.SaveChanges();
            }

            return new JsonDotNetResult(sytemapp);
        }
    }
}