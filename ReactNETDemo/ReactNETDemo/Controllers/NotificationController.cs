﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ReactNETDemo.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class NotificationController : Controller
    {
        // GET: Notification
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetNumDeliverablesNotifications()
        {
            IList<Note> notes= null;
            using (var context = new FollowingContext())
            {
                //context.Configuration.ProxyCreationEnabled = false;
                notes = (from s in context.Notes
                         where s.IsAttended==false
                          select s).ToList();
            }

            return new JsonDotNetResult(notes.Count);
        }


        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetNumNotesUnattended(string userNickName)
        {
            IList<Note> notes = null;
            using (var context = new FollowingContext())
            {
                //context.Configuration.ProxyCreationEnabled = false;
                notes = (from s in context.Notes
                         where s.IsAttended == false && s.UserNoteRespNickname==userNickName
                         select s).ToList();
            }

            return new JsonDotNetResult(notes.Count);
        }

        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetNumVoBosPerDay()
        {
            IList<VoBo> vobos = null;
            using (var context = new FollowingContext())
            {
                //context.Configuration.ProxyCreationEnabled = false;
                vobos = (from s in context.VoBos
                         where s.DateVoboReigster == DateTime.Today
                         select s).ToList();
            }

            return new JsonDotNetResult(vobos.Count);
        }
    }
}