﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class ReleaseController : Controller
    {
        // GET: Release
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllReleases()
        {
            IList<Release> releases = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                releases = (from p in context.Releases
                            orderby p.ReleaseId descending
                            select p).ToList();

            }
            //Parse the object to Json
            return new JsonDotNetResult(releases);
        }

        [HttpPost]
        public ActionResult AddNewRelease(Release release)
        {
            using (var context = new FollowingContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Releases.Add(release);
                context.SaveChanges();
            }
            return new JsonDotNetResult(release);
        }
    }
}