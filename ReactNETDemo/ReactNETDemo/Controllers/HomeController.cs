﻿using FollowingModel;
using ReactNETDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ReactNETDemo.Controllers
{
    public class HomeController : Controller
    {
        private static readonly IList<CommentModel> _comments;

        static HomeController()
        {
            _comments = new List<CommentModel>
            {
                new CommentModel
                {
                    Id = 1,
                    Author = "Daniel Lo NigroFM",
                    Text = "Hello ReactJS.NET World! - New"
                },
                new CommentModel
                {
                    Id = 2,
                    Author = "Pete HuntFM",
                    Text = "This is one comment - New"
                },
                new CommentModel
                {
                    Id = 3,
                    Author = "Jordan WalkeFM",
                    Text = "This is *another* comment - New"
                },
            };
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Comments()
        {
            return Json(_comments, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddComment(CommentModel comment)
        {
            // Create a fake ID for this comment
            comment.Id = _comments.Count + 1;
            _comments.Add(comment);
            return Content("Success :)");
        }

    }
}