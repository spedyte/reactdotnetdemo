﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class DeliveryController : Controller
    {
        // GET: Delivery
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAllDeliveriesByProject(int projectId,int releaseId)
        {
            IList<MileStone> deliveries = null;
            if (projectId != 0 && releaseId != 0)
                using (var context = new FollowingContext())
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    context.Configuration.ProxyCreationEnabled = false;
                    deliveries = (from p in context.MileStones.Include("Project").Include("Release").Include("Template")
                                  join e in context.Projects on p.ProjectId equals e.ProjectId
                                  where p.ProjectId == projectId && p.Release.ReleaseId == releaseId
                                  orderby p.MileStoneId descending
                                  select p).ToList();

                }
            else if (projectId == 0 && releaseId != 0)
                using (var context = new FollowingContext())
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    context.Configuration.ProxyCreationEnabled = false;
                    deliveries = (from p in context.MileStones.Include("Project").Include("Release").Include("Template")
                                  join e in context.Releases on p.Release.ReleaseId equals e.ReleaseId
                                  where p.Release.ReleaseId == releaseId
                                  orderby p.MileStoneId descending
                                  select p).ToList();

                }
            else if(projectId != 0 && releaseId == 0)
                using (var context = new FollowingContext())
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    context.Configuration.ProxyCreationEnabled = false;
                    deliveries = (from p in context.MileStones.Include("Project").Include("Release").Include("Template")
                                  join e in context.Projects on p.ProjectId equals e.ProjectId
                                  where p.ProjectId == projectId
                                  orderby p.MileStoneId descending
                                  select p).ToList();

                }
                        
            //Parse the object to Json
            return new JsonDotNetResult(deliveries);
        }
    }
}