﻿using FollowingModel;
using ReactNETDemo.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReactNETDemo.Controllers
{
    public class PositionController : Controller
    {
        // GET: Position
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action to retrieve all the position availables for the administrator
        /// to create new users
        /// </summary>
        /// <returns>Json Object List with the information of the positions</returns>
        public ActionResult GetAllPositions()
        {
            IList<Position> postions = null;

            using (var context = new FollowingContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                postions = (from p in context.Positions
                            where p.Name!="Administrator"
                            orderby p.PositionId
                             select p).ToList();
            }

            //Parse the object to Json
            return new JsonDotNetResult(postions);
        }
    }
}