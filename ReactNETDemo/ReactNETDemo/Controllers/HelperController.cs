﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ReactNETDemo.Controllers
{
    public class HelperController : Controller
    {
        // GET: Helper
        public ActionResult Index()
        {
            return View();
        }

        //[OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetAvatars()
        {
            DirectoryInfo directory = new DirectoryInfo(Server.MapPath(@"~\w3images"));
            var files = directory.GetFiles("avatar*").ToList();
            string[] filesName = new string[files.Count];
            for (int i = 0; i < files.Count(); i++)
            {
                filesName[i] = files.ElementAt(i).Name;
            }

            return Json(filesName, JsonRequestBehavior.AllowGet);
        }
        
    }
}